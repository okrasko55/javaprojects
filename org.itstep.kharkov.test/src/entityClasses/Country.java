/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entityClasses;

/**
 *
 * @author Oleg K
 */
public class Country {
    private int countryId;
    private String countryName;
    private int regionId;
    private Region region;

    public Country(int countryId, String countryName, int regionId) {
        this.countryId = countryId;
        this.countryName = countryName;
        this.regionId = regionId;
    }
    
    public Country(int countryId, String countryName, Region region) {
        this.countryId = countryId;
        this.countryName = countryName;
        this.region = region;
        this.regionId = region.getRegionId();
    }



    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int country_Id) {
        this.countryId = country_Id;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String country_name) {
        this.countryName = country_name;
    }

    public int getRegionId() {
        return regionId;
    }

    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
        this.setRegionId(region.getRegionId());
    }
}
