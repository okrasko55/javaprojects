/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entityClasses;

/**
 *
 * @author Oleg K
 */
public class Region {

    private int regionId;
    private String regionName;

    public Region(int regionId, String regionName) {
        this.regionId = regionId;
        this.regionName = regionName;
    }

    public int getRegionId() {
        return regionId;
    }

    public void setRegionId(int region_id) {
        this.regionId = region_id;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String region_name) {
        this.regionName = region_name;
    }

    @Override
    public String toString() {
        return this.regionName;
    }
}
