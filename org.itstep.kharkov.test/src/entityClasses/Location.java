/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entityClasses;

/**
это Entity-класс:
1)закрытый конструктор по умолчанию
2)поля полностью соответствуют структуре таблицы в БД
3)для каждого поля есть set\get метод
4)класс должен быть экземпляром любой коллекции:
* -toString()-для тотбражения в интерфейсных компонентах
* -equals: работать будут Contains
* -toCompare : работать будут Sort и Search
 */
public class Location implements Comparable<Location> {
    private Long locationId;
    private String streetAddress;
    private String postalCode;
    private String city;
    private String stateProvince;
    private String countryId;
    
    private Location()
    {
        
    }
    public Location(Long locationId, String streetAddress, String postalCode, String city, String stateProvince, String countryId) {
        this.locationId = locationId;
        this.streetAddress = streetAddress;
        this.postalCode = postalCode;
        this.city = city;
        this.stateProvince = stateProvince;
        this.countryId = countryId;
    }
    public String toString()
    {
      return String.format("(%d,%s,%s,%s,%s,%s)", locationId, streetAddress, postalCode,city,stateProvince,countryId);
    }
    public Long getLocationId() {
        return locationId;
    }
    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }
    public String getStreetAddress() {
        return streetAddress;
    }
    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }
    public String getPostalCode() {
        return postalCode;
    }
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getStateProvince() {
        return stateProvince;
    }
    public void setStateProvince(String stateProvince) {
        this.stateProvince = stateProvince;
    }
    public String getCountryId() {
        return countryId;
    }
    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }
   @Override
    public boolean equals(Object o) {
        //сравниваем с null
        //совместимость типа
        //сравниваем ссылки - не сам ли объект пришел
        //логика различия: сравниваем поля
        boolean result = false;
        if (o != null)
        {
            if ( o instanceof Location)
            {
                if ( o != this)
                {
                    Location other = (Location)o;
                    boolean fid = (this.getLocationId()==other.getLocationId())?true:false;
                    result = fid;
                }
            }
        }
        return result; //To change body of generated methods, choose Tools | Templates.
    }
    @Override
    public int compareTo(Location location) {
       return (int)(location.locationId - this.locationId);
    }
    /*
     int res = String.Compare(str, str2);
            switch (res)
            {
                case -1:
                    MessageBox.Show("s1 < s2");
                    break;
                case 1:
                    MessageBox.Show("s1 > s2");
                    break;
                default://0
                    MessageBox.Show("s1 == s2");
                    break;
            }
    */

}
