/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testprocedureoracle;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import entityClasses.Employee;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author asp
 */
public class TestProcedureOracle {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        driverClassRegistration();
        Connection connection = createOracleConnection();
//        addBans(connection);
        testSelectBanks(connection);
    }
    
    private static void addBans(Connection connection){
           try {
            System.out.println("Creating statement...");
            //вызов процедуры по синтаксису ODBC(так вызываются из любых языков(C#, PHP))
            String sql = "{call HR.ADD_BANKS (?, ?, ?)}";
            CallableStatement stmt = connection.prepareCall(sql);
            String IN_PARAM1 = "CITY BANK";
            stmt.setString(1, IN_PARAM1);
            String IN_PARAM2 = "NY CITY";
            stmt.setString(2, IN_PARAM2);
            stmt.registerOutParameter(3, java.sql.Types.INTEGER);
//Use execute method to run stored procedure.
            System.out.println("Executing stored function...");
            stmt.execute();
//Retrieve employee name with getXXX method
            int outParamValue = stmt.getInt(3);
            System.out.println("output param value is " + outParamValue);
            stmt.close();
            connection.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private static void addClients(Connection connection) {
        try {
            System.out.println("Creating statement...");
            //вызов процедуры по синтаксису ODBC(так вызываются из любых языков(C#, PHP))
            String sql = "{call HR.ADD_CLIENTS (?,?,?, ?)}";
            CallableStatement stmt = connection.prepareCall(sql);
            String IN_PARAM1 = "Petr";
            stmt.setString(1, IN_PARAM1);
            String IN_PARAM2 = "333333";
            stmt.setString(2, IN_PARAM2);
            String IN_PARAM3 = "petr@mail";
            stmt.setString(3, IN_PARAM3);
            stmt.registerOutParameter(4, java.sql.Types.INTEGER);
//Use execute method to run stored procedure.
            System.out.println("Executing stored function...");
            stmt.execute();
//Retrieve employee name with getXXX method
            int outParamValue = stmt.getInt(4);
            System.out.println("output param value is " + outParamValue);
            stmt.close();
            connection.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }   //
    }

    private static List<Employee> testSelectEmployees(Connection connection) {
        List<Employee> list = null;
        if (connection != null) {
            System.out.println("You made it, take control your database now!");
            try {
                Statement st = connection.createStatement();
                ResultSet rs = st.executeQuery("SELECT * FROM HR.EMPLOYEES");
                list = new ArrayList<>();
                while (rs.next()) {
                    String name = rs.getString("LAST_NAME");
                    int salary = rs.getInt("SALARY");
                    int id = rs.getInt("EMPLOYEE_ID");
                    list.add(new Employee(name, salary, id));
                }
            } catch (SQLException ex) {
                System.out.println("executeQuery Failed! Check output console");
                ex.printStackTrace();
            }
        }return list;
    }
    
    private static void testSelectBanks(Connection connection) {
        if (connection != null) {
            System.out.println("You made it, take control your database now!");
            try {
                Statement st = connection.createStatement();
                ResultSet rs = st.executeQuery("SELECT * FROM HR.BANKS");
                while (rs.next()) {
                    int id = rs.getInt("BANKS_ID");
                    String name = rs.getString("BANKS_NAME");
                    String region = rs.getString("REGION_INFO");
                    System.out.println("id = " + id + " name = " + name + " region = " + region);
                }
            } catch (SQLException ex) {
                System.out.println("executeQuery Failed! Check output console");
                ex.printStackTrace();
                return;
            }
        }
    }

    private static Connection createOracleConnection() {
        Connection connection = null;
        try {
            // ORA-12705: Cannot access NLS
            // data files or invalid environment specified
            java.util.Locale locale = java.util.Locale.getDefault();
            java.util.Locale.setDefault(java.util.Locale.ENGLISH);
            
            connection = DriverManager.getConnection(
                    "jdbc:oracle:thin:@localhost:1521:xe", "system", "123");
            
            java.util.Locale.setDefault(locale);
        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
        }
        
        return connection;
    }
    

    private static boolean driverClassRegistration() {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your Oracle JDBC Driver?");
            e.printStackTrace();
            return true;
        }
        System.out.println("Oracle JDBC Driver Registered!");
        return false;
    } 
    
 
}
