/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testhibernatejpa;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Oleg K
 */
public class TestHibernateJPA {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        testEmployee("Ivan");
        testSelectEmployee();
    }
    
    
    private static void testEmployee(String name) throws HibernateException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Employee emp = new Employee();
        emp.setName(name);
        session.save(emp);
        session.getTransaction().commit();
        session.close();
    }
    
    private static void testSelectEmployee() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        String hql = "from Employee";
        Query query = session.createQuery(hql);
        List<Employee> listEmployee = query.list();
        for (Employee employee : listEmployee) {
            System.out.println(employee.getName());
        }
        session.close();
    }
}
