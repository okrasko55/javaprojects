<%-- 
    Document   : productsSelectResponse
    Created on : 04.09.2017, 11:35:38
    Author     : Oleg K
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>


<sql:setDataSource var="snapshot" driver="org.sqlite.JDBC"
     url="jdbc:sqlite:D:/Java Projects/JSPAndSQL/build/web/AdminPanel.db"
     user=""  password=""
     />

<sql:query dataSource="${snapshot}" var="result">
SELECT * from Products;
</sql:query>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="styles.css">
        <title>Page for products</title>
    </head>
    <body>
        <h1>List of products :</h1>
        <div class="fileform">
            <table border="1" width="100%">
                <tr>
                    <th>Products ID</th>
                    <th>Name</th>
                    <th>Price</th>
                </tr>
                <c:forEach var="row" items="${result.rows}">
                    <tr>
                        <td><c:out value="${row.products_id}"/></td>
                        <td><c:out value="${row.products_name}"/></td>
                        <td><c:out value="${row.price}"/></td>
                    </tr>
                </c:forEach>
            </table>
            <form action="index.jsp">
                <input id="submit" type="submit" value="back" />
            </form>
        </div>
    </body>
</html>
