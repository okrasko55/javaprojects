<%-- 
    Document   : index
    Created on : 04.09.2017, 11:14:59
    Author     : Oleg K
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="styles.css">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="productsSelectResponse.jsp">
            <h3>Check list of products!</h3>
            <input id="submit" value="select" type="submit" name="buttonSelectProducts"/>
        </form>
    </body>
</html>
