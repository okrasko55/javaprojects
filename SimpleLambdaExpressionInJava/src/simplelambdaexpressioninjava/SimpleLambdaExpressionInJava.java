package simplelambdaexpressioninjava;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;


public class SimpleLambdaExpressionInJava {
    //примеры функций предикатов
    static boolean isEven(int num) {
        return (num % 2) == 0; // simple
    }
    static boolean isOdd(int num) {
        return (num % 2) == 1; // simple
    }
    public static void printIntegers(List<Integer> numbers) {
        for (int number : numbers) {
            if (isEven(number)) 
            {
                System.out.println(number);
            }
        }
    }
    public static void printIntegers(List<Integer> numbers,
            Predicate<Integer> pred) {
        for (int number : numbers) {
            if (pred.test(number)) {
                System.out.printf("%d ",number);
            }
        }
        System.out.println();
    }
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5,
                6, 7, 8, 9, 10,-1,-2,-3);
        //Пример 1 обощение предикатов. 
        printIntegers(numbers);
        Predicate<Integer> pred = num -> (num % 2) == 0;
        printIntegers(numbers, pred);
        printIntegers(numbers, num -> (num % 2) != 0);
        printIntegers(numbers, num -> num < 0);
        printIntegers(numbers, num -> num > 0);
        //Пример 2 Обобщение вывода на экран
        for (int number : numbers) {
            System.out.println(number);
        }
        numbers.forEach((Integer value) -> {System.out.println(value);});
        numbers.forEach(value -> System.out.println(value));
        numbers.forEach(System.out::println);
        //Пример 3 Обощение суммирования в предикатах
        int r = sumAll(numbers, n -> true);
        r = sumAll(numbers, n -> n % 2 == 0);
        r = sumAll(numbers, n -> n > 3);
    }
    public static int sumAll(List<Integer> numbers,
            Predicate<Integer> p) {
    int total = 0;
    for (int number : numbers) {
        if (p.test(number)) {
            total += number;
        }
    }
    return total;
}
}
