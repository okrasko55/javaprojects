<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page: registration</title>
    </head>
    <body>
        <h1>User Registration</h1>
        <c:if test="${empty param.txtUser}">
            <c:out value="User not empty"></c:out>
        </c:if>
        <c:if test="${not empty param.txtUser}">
                    <c:if test="${param.txtUser == 'admin' and param.txtPassword == 'admin'}">
                        <jsp:forward page="ok.jsp"/>
                    </c:if>
        </c:if>
        <form>
        <table>
            <tr>
                <td>Username</td>
                <td><input type="text" name="txtUser"/></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="text" name="txtPassword"/></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="Submit" value="Login"/> </td>
            </tr>
        </table>
            </form>
    </body>
</html>