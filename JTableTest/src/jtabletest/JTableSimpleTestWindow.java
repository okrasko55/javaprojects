/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtabletest;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author asp
 */
public class JTableSimpleTestWindow extends JFrame{
    public JTableSimpleTestWindow(){
        this.setTitle("JTable for User class");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(300, 300);
        
        String [] columns = {"id", "FIO", "Salary per hour", "Совместитель"};
        final Class[] columnClass = {Integer.class, String.class, Double.class, Boolean.class};
        Object[][] data = {
            {1, "Ivanov", 40.0, false},
            {2, "Petrov", 70.0, false},
            {3, "Sidorov", 60.0, true}
        };
        
        DefaultTableModel model = new DefaultTableModel(data, columns) {
            @Override
            public Class<?> getColumnClass(int i) {
                return columnClass[i]; //To change body of generated methods, choose Tools | Templates.
            }
        };
        JTable jTable = new JTable(model);
        this.add(new JScrollPane(jTable));
        
        this.pack();
        this.setVisible(true);
    }
}
