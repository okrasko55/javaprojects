/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import entityClasses.Region;
import java.util.List;

/**
 *
 * @author Oleg K
 */
public interface DaoRegion {
    public void createRegion(Region region);
    public List<Region> getRegions();
    public Region getRegion(int regionId);
    public void updateLocation(Region region);
    public void deleteLocation(int regionId);
}
