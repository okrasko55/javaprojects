/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import entityClasses.Country;
import entityClasses.Location;
import java.util.List;

/**
 *
 * @author Oleg K
 */
public interface DaoCountry {
    public void createCountry(Country country);
    public List<Country> getCountries();
    public Country getCountry(int countryId);
    public void updateCountry(Country country);
    public void deleteCountry(int countryId);
}
