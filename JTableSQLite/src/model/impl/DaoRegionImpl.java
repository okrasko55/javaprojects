/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.impl;

import entityClasses.Country;
import entityClasses.Region;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.dao.DaoRegion;
import static model.impl.DaoCountryImpl.CREATE_SQL;
import static model.impl.DaoCountryImpl.DELETE_SQL;
import static model.impl.DaoCountryImpl.GET_ALL_SQL;
import static model.impl.DaoCountryImpl.GET_SQL;
import static model.impl.DaoCountryImpl.UPDATE_SQL;

/**
 *
 * @author Oleg K
 */
public class DaoRegionImpl implements DaoRegion{
    
    public static final String CREATE_SQL = "INSERT INTO REGIONS (REGION_NAME) "
            + "VALUES (?, ?)";
    public static final String GET_ALL_SQL = "SELECT REG.REGION_ID, REG.REGION_NAME FROM REGIONS REG";
    public static final String GET_SQL = "SELECT REG.REGION_ID, REG.REGION_NAME FROM REGIONS REG WHERE REG.REGION_ID = ?";
    public static final String DELETE_SQL = "DELETE FROM REGIONS WHERE REGION_ID = ?";
    public static final String UPDATE_SQL = "UPDATE REGIONS SET REGION_ID = ?, REGION_NAME = ?"
            + "WHERE REGION_ID = ?";

    @Override
    public void createRegion(Region region) {
        try {
            Connection connection = DaoLocationImpl.getConnection();
            //COUNTRY_ID,COUNTRY_NAME,REGION_ID
            PreparedStatement st = connection.prepareStatement(CREATE_SQL);
            st.setString(1, region.getRegionName());
            st.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("executeQuery Failed! Check output console");
            ex.printStackTrace();
            return;
        }
    }

    @Override
    public List<Region> getRegions() {
        List<Region> regions = new ArrayList<Region>();
        //COUNTRY_ID,COUNTRY_NAME,REGION_ID,REGION_NAME
        try {
            Connection connection = DaoLocationImpl.getConnection();
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(GET_ALL_SQL);
            while (rs.next()) {
                int regionId = rs.getInt("REGION_ID");
                String regionName = rs.getString("REGION_NAME");
                Region region = new Region(regionId, regionName);
                regions.add(region);
            }
        } catch (SQLException ex) {
            System.out.println("executeQuery Failed! Check output console");
            ex.printStackTrace();
            return regions;
        }
        return regions;
    }

    @Override
    public Region getRegion(int regionId) {
        List<Region> regions= new ArrayList<Region>();
        try {
            Connection connection = DaoLocationImpl.getConnection();
            PreparedStatement st = connection.prepareStatement(GET_SQL);
            st.setInt(1, regionId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("REGION_ID");
                String regionName = rs.getString("REGION_NAME");
                Region region = new Region(id, regionName);
                regions.add(region);
            }
        } catch (SQLException ex) {
            System.out.println("executeQuery Failed! Check output console");
            ex.printStackTrace();
            Region region2 = new Region(0, "");
            return region2;
        }
        if (regions.isEmpty()) {
            Region region2 = new Region(0, "");
            return region2;
        } else {
            return regions.get(0);
        }
    }

    @Override
    public void updateLocation(Region region) {
        try {
            Connection connection = DaoLocationImpl.getConnection();
            PreparedStatement st = connection.prepareStatement(UPDATE_SQL);
            st.setInt(1, region.getRegionId());
            st.setString(2, region.getRegionName());
            st.setInt(3, region.getRegionId());
            st.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("executeQuery Failed! Check output console");
            ex.printStackTrace();
            return;
        }
    }

    @Override
    public void deleteLocation(int regionId) {
        try {
            Connection connection = DaoLocationImpl.getConnection();
            PreparedStatement st = connection.prepareStatement(DELETE_SQL);
            st.setInt(1, regionId);
            st.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("executeQuery Failed! Check output console");
            ex.printStackTrace();
            return;
        }
    }
    
}
