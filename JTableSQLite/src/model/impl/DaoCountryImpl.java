/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.impl;

import entityClasses.Country;
import entityClasses.Region;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.dao.DaoCountry;

/**
 *
 * @author Oleg K
 */
public class DaoCountryImpl implements DaoCountry {

    public static final String CREATE_SQL = "INSERT INTO COUNTRIES (COUNTRY_NAME,REGION_ID) "
            + "VALUES (?,?)";
    public static final String GET_ALL_SQL = "SELECT CTR.COUNTRY_ID, CTR.COUNTRY_NAME, CTR.REGION_ID, REG.REGION_NAME FROM COUNTRIES CTR LEFT JOIN REGIONS REG ON CTR.REGION_ID=REG.REGION_ID";
    public static final String GET_SQL = "SELECT CTR.COUNTRY_ID, CTR.COUNTRY_NAME, CTR.REGION_ID, REG.REGION_NAME FROM COUNTRIES CTR LEFT JOIN REGIONS REG ON CTR.REGION_ID=REG.REGION_ID WHERE CTR.COUNTRY_ID = ?";
    public static final String DELETE_SQL = "DELETE FROM COUNTRIES WHERE COUNTRY_ID = ?";
    public static final String UPDATE_SQL = "UPDATE COUNTRIES SET COUNTRY_NAME = ?, REGION_ID=?, COUNTRY_ID = ? "
            + "WHERE COUNTRY_ID = ?";

    @Override
    public void createCountry(Country country) {
        try {
            Connection connection = DaoLocationImpl.getConnection();
            //COUNTRY_ID,COUNTRY_NAME,REGION_ID
            PreparedStatement st = connection.prepareStatement(CREATE_SQL);
            String country_Name = country.getCountryName();
            int regionId = country.getRegionId();
            st.setString(1, country_Name);
            st.setLong(2, regionId);
            st.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("executeQuery Failed! Check output console");
            ex.printStackTrace();
            return;
        }
    }

    @Override
    public List<Country> getCountries() {
        List<Country> countries = new ArrayList<Country>();
        //COUNTRY_ID,COUNTRY_NAME,REGION_ID,REGION_NAME
        try {
            Connection connection = DaoLocationImpl.getConnection();
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(GET_ALL_SQL);
            while (rs.next()) {
                int regionId = rs.getInt("REGION_ID");
                String countryName = rs.getString("COUNTRY_NAME");
                String regionName = rs.getString("REGION_NAME");
                int countryId = rs.getInt("COUNTRY_ID");
                Region region = new Region(regionId, regionName);
                Country country = new Country(countryId, countryName, region);
                countries.add(country);
            }
        } catch (SQLException ex) {
            System.out.println("executeQuery Failed! Check output console");
            ex.printStackTrace();
            return countries;
        }
        return countries;
    }
   

    @Override
    public Country getCountry(int countryId) {
        List<Country> countries = new ArrayList<Country>();
        try {
            Connection connection = DaoLocationImpl.getConnection();
            PreparedStatement st = connection.prepareStatement(GET_SQL);
            st.setInt(1, countryId);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int regionId = rs.getInt("REGION_ID");
                String countryName = rs.getString("COUNTRY_NAME");
                String regionName = rs.getString("REGION_NAME");
                Region region = new Region(regionId, regionName);
                Country country = new Country(countryId, countryName, region);
                countries.add(country);
            }
        } catch (SQLException ex) {
            System.out.println("executeQuery Failed! Check output console");
            ex.printStackTrace();
            Region region2 = new Region(0, "");
            Country country2 = new Country(0, "", region2);
            return country2;
        }
        if (countries.isEmpty()) {
            Region region2 = new Region(0, "");
            Country country2 = new Country(0, "", region2);
            return country2;
        } else {
            return countries.get(0);
        }
    }

    @Override
    public void updateCountry(Country country) {
        try {
            Connection connection = DaoLocationImpl.getConnection();
            PreparedStatement st = connection.prepareStatement(UPDATE_SQL);
            st.setString(1, country.getCountryName());
            st.setInt(2, country.getRegionId());
            st.setInt(3, country.getCountryId());
            st.setInt(4, country.getCountryId());
            st.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("executeQuery Failed! Check output console");
            ex.printStackTrace();
            return;
        }
    }

    @Override
    public void deleteCountry(int countryId) {
        try {
            Connection connection = DaoLocationImpl.getConnection();
            PreparedStatement st = connection.prepareStatement(DELETE_SQL);
            st.setInt(1, countryId);
            st.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("executeQuery Failed! Check output console");
            ex.printStackTrace();
            return;
        }
    }
}
