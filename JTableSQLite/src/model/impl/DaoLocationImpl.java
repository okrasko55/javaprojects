/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.impl;

import entityClasses.Location;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.dao.DaoLocation;

/**
 *
 * @author Oleg K
 */
public class DaoLocationImpl implements DaoLocation {

    private static final String CREATE_SQL = "INSERT INTO LOCATIONS( STREET_ADDRESS, POSTAL_CODE, CITY, "
            + "STATE_PROVINCE, COUNTRY_ID) "
            + "VALUES (?, ?, ?, "
            + "?, ?)";

    private static final String GET_ALL_SQL = "SELECT LOCATION_ID, STREET_ADDRESS, POSTAL_CODE, CITY, STATE_PROVINCE, COUNTRY_ID "
            + "FROM LOCATIONS";

    private static final String UPDATE_SQL = "UPDATE LOCATIONS SET STREET_ADDRESS = ?, POSTAL_CODE=?, "
            + "CITY = ?, STATE_PROVINCE = ?, COUNTRY_ID = ? "
            + "WHERE LOCATION_ID = ?";
    
    private static final String GET_SQL = "SELECT LOCATION_ID, STREET_ADDRESS, POSTAL_CODE, CITY, STATE_PROVINCE, COUNTRY_ID "
            + "FROM LOCATIONS WHERE LOCATION_ID = ?";

    public static Connection getConnection() {
        Connection connection;
        System.out.println("-------- SQLite JDBC Connection Testing ------");
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your SQLite JDBC Driver?");
            e.printStackTrace();
            return null;
        }
        System.out.println("SQLite JDBC Driver Registered!");
        try {

            connection = DriverManager.getConnection(
                    "jdbc:sqlite:jtable.db");
        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return null;
        }
        return connection;
    }

    @Override
    public void createLocation(Location location) {
        try {
            Connection connection = getConnection();
            PreparedStatement st = connection.prepareStatement(CREATE_SQL);
            st.setString(1, location.getStreetAddress());
            st.setString(2, location.getPostalCode());
            st.setString(3, location.getCity());
            st.setString(4, location.getPostalCode());
            st.setString(5, location.getCountryId());
            st.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("executeQuery Failed! Check output console");
            ex.printStackTrace();
            return;
        }
    }

    @Override
    public List<Location> getLocations() {
        List<Location> locations = new ArrayList<Location>();
        try {
            Connection connection = getConnection();
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(GET_ALL_SQL);
            while (rs.next()) {
                long locationId = rs.getInt("LOCATION_ID");
                String streetAddress = rs.getString("STREET_ADDRESS");
                String postalCode = rs.getString("POSTAL_CODE");
                String city = rs.getString("CITY");
                String stateProvince = rs.getString("STATE_PROVINCE");
                String countryId = rs.getString("COUNTRY_ID");
                Location location = new Location(locationId, streetAddress, postalCode, city, stateProvince, countryId);
                locations.add(location);
            }
        } catch (SQLException ex) {
            System.out.println("executeQuery Failed! Check output console");
            ex.printStackTrace();
            return locations;
        }
        return locations;
    }

    @Override
    public Location getLocation(Long locationId) {
        List<Location> locations = new ArrayList<Location>();
        try {
            Connection connection = getConnection();
            PreparedStatement st = connection.prepareStatement(GET_SQL);
            st.setLong(1, locationId);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                String streetAddress = rs.getString("STREET_ADDRESS");
                String postalCode = rs.getString("POSTAL_CODE");
                String city = rs.getString("CITY");
                String stateProvince = rs.getString("STATE_PROVINCE");
                String countryId = rs.getString("COUNTRY_ID");
                Location location = new Location(locationId, streetAddress, postalCode, city, stateProvince, countryId);
                locations.add(location);
            }
        } catch (SQLException ex) {
            System.out.println("executeQuery Failed! Check output console");
            ex.printStackTrace();
            Location location2 = new Location(-1L, "", "", "", "", "");
            return location2;
        }
        return locations.get(0);
    }

    @Override
    public void updateLocation(Location location) {
        try {
            Connection connection = getConnection();
            PreparedStatement st = connection.prepareStatement(UPDATE_SQL);
            st.setString(1, location.getStreetAddress());
            st.setString(2, location.getPostalCode());
            st.setString(3, location.getCity());
            st.setString(4, location.getPostalCode());
            st.setString(5, location.getCountryId());
            st.setLong(6, location.getLocationId());
            st.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("executeQuery Failed! Check output console");
            ex.printStackTrace();
            return;
        }
    }

    @Override
    public void deleteLocation(Long locationId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
