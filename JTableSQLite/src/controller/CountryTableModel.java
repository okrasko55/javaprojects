/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entityClasses.Country;
import entityClasses.Region;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import model.dao.DaoCountry;
import model.impl.DaoCountryImpl;

/**
 *
 * @author Oleg K
 */
public class CountryTableModel extends AbstractTableModel{
    
    DaoCountry daoCountry = new DaoCountryImpl();
    
    private String[] columnNames = {"Id.", "Название Страны", "Region"};
    private List<Country> listCountry;

    public CountryTableModel(List<Country> listPerson) {
        this.listCountry = listPerson;
    }

    @Override
    public int getRowCount() {
        return listCountry.size();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return (columnIndex != 0) ? true : false;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return getValueAt(0, columnIndex).getClass();
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }
    
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        Country country = listCountry.get(rowIndex);

        switch (columnIndex) {
            case 0:
                country.setCountryId((int) value);
                break;
            case 1:
                country.setCountryName((String) value);
                break;
            case 2:
                country.setRegion((Region) value);
                break;
        }
        daoCountry.updateCountry(country);
    }


    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object returnValue = null;
        Country country = listCountry.get(rowIndex);

        switch (columnIndex) {
            case 0:
                returnValue = country.getCountryId();
                break;
            case 1:
                returnValue = country.getCountryName();
                break;
            case 2:
                returnValue = country.getRegion();
                break;
        }
        return returnValue;
    }

    public void addRow(Country newCountry) {
        daoCountry.createCountry(newCountry);
    }

    public void removeRow(int selectedRow) {
        Country country = listCountry.get(selectedRow);
        daoCountry.deleteCountry(country.getCountryId());
    }

}
