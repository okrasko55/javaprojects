/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaoracleconnectiontest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

/**
 *
 * @author Oleg K
 */
public class JavaOracleConnectionTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (ClassNotFoundException e) {
            System.out.println("Driver is not found!");
            e.printStackTrace();
            return;
        }
        
        Connection conn = null;
        
        try {
            java.util.Locale locale = java.util.Locale.getDefault();
            java.util.Locale.setDefault(java.util.Locale.ENGLISH);
            
            conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "123");
            
            java.util.Locale.setDefault(locale);
        } catch (SQLException e) {
            System.out.println("Connection is failed!");
            e.printStackTrace();
            return;
        }
        
        if (conn != null) {
            try {
                Statement stmt = conn.createStatement();
                stmt.executeUpdate("INSERT INTO HR.BANKS (BANKS_ID, BANKS_NAME,REGION_INFO) "
                        + "values (BANKS_SEQUENCE.NEXTVAL,'City-Bank','NY City')");
                ResultSet rs = stmt.executeQuery("SELECT * FROM HR.BANKS");
                while (rs.next()) {
                    int id = rs.getInt("BANKS_ID");
                    String name = rs.getString("BANKS_NAME");
                    String info = rs.getString("REGION_INFO");
                    System.out.println("id = " + id + " name = " + name + " info = " + info);
                }
            } catch (SQLException e) {
                System.out.println("executeQuery Failed! Check output console");
                e.printStackTrace();
                return;
            }
        }
    }
    
}
