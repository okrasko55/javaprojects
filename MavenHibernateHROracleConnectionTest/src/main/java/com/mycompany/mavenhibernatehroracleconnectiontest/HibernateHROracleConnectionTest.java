/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenhibernatehroracleconnectiontest;

/**
 *
 * @author asp
 */
import java.util.ArrayList;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import org.hibernate.Query;
import java.util.List;

public class HibernateHROracleConnectionTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("Nbeans + Hibernate + SQLite3 + JPA-mapping");
        testEmployeesEx();
    }
     private static void testEmployeesEx() throws ExceptionInInitializerError, HibernateException {
        SessionFactory mFctory;
        try{
            mFctory = new Configuration().configure().buildSessionFactory();
        }catch (Throwable ex) {
            System.err.println("Couldn't create session factory." + ex);
            throw new ExceptionInInitializerError(ex);
        }
        Session session = null;
        Transaction tx = null;
        //
        session = mFctory.openSession();
        System.out.println("--- Find all Employees ---");
        Query query = session.createQuery("SELECT e FROM Employees e");
	List<Employees> employees = query.list();
	for (Employees foundEmployee : employees) 
        {
	System.out.println(String.format("Found: %s\n", foundEmployee));
        
        //for (Departments dep : foundEmployee.getDepartments()) 
        Departments dep = foundEmployee.getDepartments();
        {
	System.out.println(String.format("deps info: %s\n", dep));
        }
        //
        }
        session.close();
        //
    }
}
