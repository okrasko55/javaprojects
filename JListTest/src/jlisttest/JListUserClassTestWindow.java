/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jlisttest;

import entityClasses.Employee;
import java.sql.Connection;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;


import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author asp
 */
public class JListUserClassTestWindow extends JFrame{

    private JList<Employee> employee;
    
    public JListUserClassTestWindow() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(300, 300);
        this.setLocationRelativeTo(null);
        this.setTitle("JList for User class");
        
        DefaultListModel<Employee> listModel = new DefaultListModel<>();
        Employee.driverClassRegistration();
        Connection connection = Employee.createOracleConnection();
        List<Employee> list = Employee.testSelectEmployees(connection);
        for (Employee emp : list){
            listModel.addElement(emp);
        }
        
        employee = new JList<>(listModel);
        employee.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    final List<Employee> selectedValuesList = employee.getSelectedValuesList();
                    System.out.println(selectedValuesList);
                }
            }
        });
        add(new JScrollPane(employee));
        this.setVisible(true);
    }
    
}
