/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javasqlliteconnectiontest;

/**
 *
 * @author Oleg K
 */
 import java.sql.Connection;  
 import java.sql.DriverManager;  
 import java.sql.ResultSet;  
 import java.sql.Statement;  

import java.sql.SQLException;
import java.sql.DatabaseMetaData;

public class JavaSQLLiteConnectionTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ClassNotFoundException {
        // TODO code application logic here
        
        //register the driver
        String driverName = "org.sqlite.JDBC";
        Class.forName(driverName);
        
        //set of basic variables
        String DBURL = "jdbc:sqlite:testDB.db";
        Connection conn = null;
        
        //get connection
        try{
            conn = DriverManager.getConnection(DBURL);
        } catch (SQLException e){
            System.out.println("Connection failed!");
        }
        if (conn != null){
            System.err.println("There is the connection!");
            try{
                Statement stmt = conn.createStatement();
//                stmt.executeUpdate("CREATE TABLE IF NOT EXISTS CLIENTS (CLIENTS_ID numeric, CLIENTS_NAME text)");
                stmt.executeUpdate("INSERT INTO BANKS VALUES (1, 'City-Bank', 'NY City')");
                ResultSet result = stmt.executeQuery("SELECT * FROM BANKS");
                while (result.next()){
                    System.out.println(result.getString("BANKS_NAME") 
                            + "\t" + result.getString("REGION_INFO"));
                } 
            } catch (SQLException e){
                System.out.println("executeQuerry failed!");
            }
            
        }
    }
}
