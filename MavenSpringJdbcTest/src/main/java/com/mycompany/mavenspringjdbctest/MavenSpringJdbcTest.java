/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenspringjdbctest;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import oracle.jdbc.OracleDriver;
import oracle.jdbc.pool.OracleDataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

/**
 *
 * @author asp
 */
public class MavenSpringJdbcTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
        dataSource.setDriverClass(OracleDriver.class);
        dataSource.setUsername("system");
        dataSource.setUrl("jdbc:oracle:thin:@localhost:1521:xe");
        dataSource.setPassword("123");

//        
//        OracleDataSource dataSource;
//        dataSource = new OracleDataSource();
//        dataSource.setDriverType("oracle.jdbc.driver.OracleDriver");
//        dataSource.setURL("jdbc:oracle:thin:@localhost:1521:xe");
//        dataSource.setUser("sys as sysdba");
//        dataSource.setPassword("123");
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        System.out.println("Creating tables");
        try {
            jdbcTemplate.execute("alter session set current_schema=hr");
            jdbcTemplate.execute("create table hr.customers("
                    + "id integer, first_name varchar(255), last_name varchar(255))");
        } catch (Exception exx) {
            exx.printStackTrace();
        }

        String[] names = "John Woo;Jeff Dean;Josh Bloch;Josh Long".split(";");
        for (String fullname : names) {
            String[] name = fullname.split(" ");
            System.out.printf("Inserting customer record for %s %s\n", name[0], name[1]);
            jdbcTemplate.update(
                    "INSERT INTO hr.customers(first_name,last_name) values(?,?)",
                    name[0], name[1]);
        }

        System.out.println("Querying for customer records where first_name = 'Josh':");
        List<Customer> results = jdbcTemplate.query(
                "select * from hr.customers where first_name = ?", new Object[]{"Josh"},
                new RowMapper<Customer>() {
            @Override
            public Customer mapRow(ResultSet rs, int rowNum) throws SQLException {
                return new Customer(rs.getLong("id"), rs.getString("first_name"),
                        rs.getString("last_name"));
            }
        });

        for (Customer customer : results) {
            System.out.println(customer);
        }
    }
}
