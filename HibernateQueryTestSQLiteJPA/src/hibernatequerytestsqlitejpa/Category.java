
package hibernatequerytestsqlitejpa;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "CATEGORY")
//тэг @OneToMany помечает главную таблицу
//располагается рядом с get-ром коллекции
//подчиненных классов
//связь @OneToMany - это связь между полем
//в подчиненном классе и коллекцией в главном
public class Category {
	private long id;//есть в таблице CATEGORY
	private String name;//есть в таблице CATEGORY
	private Set<Product> products;//НЕТ в таблице CATEGORY

	public Category() {
	}
	
	public Category(String name) {
		this.name = name;
	}

	@Id
	@Column(name = "CATEGORY_ID")
	@GeneratedValue
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
        //тэги JPA не нужны из-за того, 
        //что имя поля в классе=имени столбца в таблице
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@OneToMany(mappedBy = "category", //category - это имя поля в подчиненном классе Product
                   cascade = CascadeType.ALL)
	public Set<Product> getProducts() {
		return products;
	}

	public void setProducts(Set<Product> products) {
		this.products = products;
	}

}