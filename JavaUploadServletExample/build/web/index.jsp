<%-- 
    Document   : index
    Created on : 19.08.2017, 18:34:39
    Author     : asp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page: File Upload Demo</title>
        <link rel="stylesheet" href="styles.css">
    </head>
    <body>
    <center>
        <form method="post" action="upload" enctype="multipart/form-data">
            <div class="fileform">
                <div class="selectbutton">Обзор</div>
                <input id="upload" type="file" name="upload" />
            </div>
            <br/><br/>
            <input type="submit" value="Upload" />
        </form>
    </center>
</body>
</html>
