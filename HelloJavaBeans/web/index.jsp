<%-- 
    Document   : index
    Created on : 19.08.2017, 16:47:48
    Author     : asp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
     <body>
        
    <jsp:useBean id="mybean" scope="session" class="org.step.test.NameHandler" />
    <jsp:setProperty name="mybean" property="name" />
    <h1>Hello, <jsp:getProperty name="mybean" property="name" />!</h1>
        <form method="get" id="NameInputForm" name="NameInputForm" action="index.jsp">
            Enter your name:
            <input type="text" name="name" />
            <input type="submit" value="OK" />
        </form>

    </body>
</html>
