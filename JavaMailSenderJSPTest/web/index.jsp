<%-- 
    Document   : index
    Created on : 07.09.2017, 11:41:42
    Author     : Oleg K
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="styles.css">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Mailing service!</h1>
        <div class="fileform">  
            <form action="mail" method="post">
                <table>
                    <tr>
                        <td>Email</td>
                        <td><input type="text" name="email"/></td>
                    </tr>
                    <tr>
                        <td>Subject</td>
                        <td><input type="text" name="subject"/></td>
                    </tr>
                    <tr>
                        <td>Text message</td>
                        <td><input type="text" name="message"/></td>
                    </tr>
                    <tr>
                        <td><input id="submit" type="Submit" value="Send"/></td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>
