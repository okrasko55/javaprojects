/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hibernatexmlmapsqlitetest;

/**
 *
 * @author java
 */

//имя класса должно быть таким же как в имя столбца в базе данных (не обязателньо, но потом нужно корректировать в xml)
public class Employee {
      
    //все поля должны совпадать но только в нижнем регистре тут в джаве
    private long id = 1L;

    private String name;

    //обязателньо должен быть конструктор без параметров, моржно и закрытый, но обязательно явно определенный
    public Employee() {
    }

    //обязательно должны блыть сеттеры и геттеры
    public Employee(String fname) {
        name = fname;
    }

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
