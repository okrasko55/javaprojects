/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xmlcreator1;

import com.sun.xml.internal.ws.util.Pool;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;


/**
 *
 * @author asp
 */
public class XMLCreator1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        User customer = new User();
        customer.setId(100);
        customer.setAge(29);
        customer.setName("Vasya");
        
        Users users = new Users();
        users.getUsers().add(customer);
        customer.setId(200);
        users.getUsers().add(customer);
        
        try{
            File file = new File("d:\\MyData\\file.xml");
            JAXBContext jAXBContext = JAXBContext.newInstance(Users.class);
            Marshaller marshaller = jAXBContext.createMarshaller();
            
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(users, file);
            marshaller.marshal(users, System.out);
        } catch (JAXBException ex) {
            ex.printStackTrace();
        }
        
        loadObjectsFromXMLFile();
    }

    private static void loadObjectsFromXMLFile() {
        try {
            File file = new File("d:\\MyData\\file.xml");
            JAXBContext jAXBContext = JAXBContext.newInstance(Users.class);
            
            Unmarshaller unmarshaller = jAXBContext.createUnmarshaller();
            Users users = (Users) unmarshaller.unmarshal(file);
            System.out.println(users.getUsers());
        } catch (JAXBException ex) {
            ex.printStackTrace();
        }
    }
    
}
