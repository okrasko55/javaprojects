/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xmlcreator1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author asp
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class User {
    
    //variables
    @XmlElement
    private String name;
    @XmlElement
    private int age;
    @XmlAttribute(name = "IDENTITY", required = true  )
    private int id;

    //constructors
    public User() {
    }
 

    @Override
    public String toString() {
        return "User{" + "name=" + name + ", age=" + age + ", id=" + id + '}';
    }

    
    //getters and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
