/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao.interfaces;

import java.util.List;

/**
 *
 * @author Oleg K
 */
public interface BaseDaoInterface<Model> {
    public List<Model> getAll();
    public Model getById(Long id);
    public void delete(Model model);
    public void add(Model model);
    public void update(Model model, Long id);
}
