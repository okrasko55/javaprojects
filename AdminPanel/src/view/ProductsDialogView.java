/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import entityClasses.Products;
import java.awt.FlowLayout;
import java.awt.Frame;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JTextField;
import model.dao.impl.BaseDaoImpl;

/**
 *
 * @author Oleg K
 */
public class ProductsDialogView extends JDialog{
    //variables
    private JButton buttonCreate = new JButton("Create");
    private JButton buttonCancel = new JButton("Cancel");
    private JTextField fieldProductsName = new JTextField(25);
    private JTextField fieldCountryPrice = new JTextField(10);
    public Products products;
    
    //constructors
    public ProductsDialogView(Frame parent, Products products){
        super(parent, "Add New Product", true);
        this.products = products;
        setLayout(new FlowLayout(FlowLayout.LEFT, 10, 10));
        
        buttonCreate.addActionListener(event -> {
            products.setProductsName(fieldProductsName.getText());
            try {
                products.setPrice(Integer.parseInt(fieldCountryPrice.getText()));
            } catch (NumberFormatException e) {
                System.out.println("Not a number");
            }
            if ((products != null) && (!products.getProductsName().equals(""))) {
               new BaseDaoImpl<>(Products.class).add(products);
            }
            setVisible(false);
        });

        // add event listener for the button Remove
        buttonCancel.addActionListener(event -> setVisible(false));

        // add components to this frame
        add(fieldProductsName);
        add(fieldCountryPrice);
        add(buttonCreate);
        add(buttonCancel);

        pack();
        setResizable(false);
        setLocationRelativeTo(parent);
    }
}
