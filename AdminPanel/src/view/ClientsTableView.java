/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.ClientsTableModel;
import controller.ProductsTableModel;
import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author Oleg K
 */
public class ClientsTableView extends JFrame{
    public ClientsTableView(){
        ClientsTableModel modelClients = new ClientsTableModel();
        JTable tableClients = new JTable(modelClients);
        JScrollPane scrollpaneClients = new JScrollPane(tableClients);
        setTitle("Clients table");
        add(scrollpaneClients, BorderLayout.EAST);
//        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }
}
