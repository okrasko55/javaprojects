/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

/**
 *
 * @author Oleg K
 */
public class GeneralView{
    //variables
    private JButton clients = new JButton("Clients table");
    private JButton employes = new JButton("Employees table");
    private JButton orders = new JButton("Orders table");
    private JButton products = new JButton("Products table");
    
    //constructors
    public GeneralView(){
        //add events
        clients.addActionListener(event -> {
            SwingUtilities.invokeLater(() -> new ClientsTableView());
        });
        employes.addActionListener(event -> {
            SwingUtilities.invokeLater(() -> new EmployeesTableView());
        });
        orders.addActionListener(event -> {
            SwingUtilities.invokeLater(() -> new OrdersTableView());
        });
        products.addActionListener(event -> {
            SwingUtilities.invokeLater(() -> new ProductsTableView());
        });

        //making frame
        JFrame frame = new JFrame("Admin Panel");
        frame.add(clients);
        frame.add(employes);
        frame.add(orders);
        frame.add(products);
        
        //layout
        frame.getContentPane().setLayout(new GridLayout(1, 4));
        frame.setSize(600, 200);
//        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
}
