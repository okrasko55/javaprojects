/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.ClientsTableModel;
import controller.OrdersTableModel;
import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author Oleg K
 */
public class OrdersTableView extends JFrame {

    public OrdersTableView() {
        OrdersTableModel model = new OrdersTableModel();
        JTable tableOrders = new JTable(model);
        JScrollPane scrollpaneClients = new JScrollPane(tableOrders);
        setTitle("Orders table");
        add(scrollpaneClients, BorderLayout.EAST);
//        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
//        setLocationRelativeTo(null);
        setVisible(true);
    }

}
