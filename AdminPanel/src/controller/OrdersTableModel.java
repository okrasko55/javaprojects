/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entityClasses.Clients;
import entityClasses.Employees;
import entityClasses.Orders;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import model.dao.impl.BaseDaoImpl;
import model.dao.interfaces.BaseDaoInterface;

/**
 *
 * @author Oleg K
 */
public class OrdersTableModel extends AbstractTableModel{
     //variables
    private final List<Orders> ordersList;
    private final BaseDaoInterface<Orders> ordersDao;
    private final String[] columnNames = new String[]{
        "Id", "Description", "Orders Date", "Total Costs", "Clients Id"
    };

    public OrdersTableModel() {
        ordersDao = new BaseDaoImpl<>(Orders.class);
        this.ordersList = ordersDao.getAll();
    }
    

     private final Class[] columnClass = new Class[]{
        Long.class, String.class, String.class, Integer.class, Clients.class
    };

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return columnClass[columnIndex];
    }

    @Override
    public int getRowCount() {
        return ordersList.size();
    }

     @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return (columnIndex == 1) ? true : false; 
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Orders row = ordersList.get(rowIndex);
         if (0 == columnIndex) {
            return row.getOrdersId();
        } else if (1 == columnIndex) {
           return row.getDescription();
        } else if (2 == columnIndex) {
            return row.getOrdersDate();
        } else if (3 == columnIndex) {
            return row.getTotalCosts();
        } else if (4 == columnIndex) {
            return row.getClients();
        }
        return null;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        Orders row = ordersList.get(rowIndex);
        if (0 == columnIndex) {
            row.setOrdersId((Long) aValue);
        } else if (1 == columnIndex) {
            row.setDescription((String) aValue);
        } else if (2 == columnIndex) {
            row.setOrdersDate((String) aValue);
        } else if (3 == columnIndex) {
            row.setTotalCosts((int) aValue);
        } else if (4 == columnIndex) {
            row.setClients((Clients) aValue);
        }
        ordersDao.update(row, row.getOrdersId());
    }
    
}
