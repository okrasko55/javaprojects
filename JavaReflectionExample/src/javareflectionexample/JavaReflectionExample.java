/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javareflectionexample;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 *
 * @author asp
 */
public class JavaReflectionExample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        User aUser = new User();
        aUser.setId(Long.MIN_VALUE);
        aUser.setEmail("tester@gmail.com");
        String userEmail = aUser.getEmail();
        //
//        try {
//            Method method = aUser.getClass().getMethod("getEmail");
//            userEmail = (String) method.invoke(aUser);
//            System.out.println(userEmail);
//        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
//            e.printStackTrace();
//        }
           Class clazz = User.class;
        Method[] methods = clazz.getMethods();
        Method method = null;
        for (Method aMethod : methods) {
            System.out.println(aMethod.getName());
        }
    }
    
}
