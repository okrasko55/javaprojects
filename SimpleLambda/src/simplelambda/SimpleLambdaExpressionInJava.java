
package simplelambda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

public class SimpleLambdaExpressionInJava {

    
    public static void main(String[] args) {
        
      List<Integer> listInt = Arrays.asList(1,4,6,2,4,7, -4, 22, -6, 0, -3);
        //printIntegers(listInt, pred -> (pred % 2) == 0);
        //System.out.println("");
        //printIntegers(listInt, pred -> (pred < 0));
        //System.out.println("");
        //printIntegers(listInt, pred -> (pred >= 0));
        
//        listInt.forEach((Integer value) -> System.out.println(value));
//        listInt.forEach(value -> System.out.println(value));
//        listInt.forEach(System.out::println);
        
        printIntegers(listInt, pred -> (pred < 0));
        printIntegers(listInt, pred -> true);
        
        //вариант с анонимным классом
         List<String> names = Arrays.asList("Петр", "Анна", "Михаил", "Евгений");
         Collections.sort(names, new Comparator<String>(){
          @Override
          public int compare(String t, String t1) {
              return t1.compareTo(t);
          }
        });
         
         //вариант с лямбдой но более грамоздкий
         Collections.sort(names, (String s1, String s2) -> {
             return s1.compareTo(s2);
         });
         
         //вариант с лямбдой но короткий
         Collections.sort(names, (s1, s2) -> s2.compareTo(s1));
         
    }

    public static void printIntegers(List<Integer> numbers, Predicate<Integer> pred){
        int sum = 0;
        for(int list : numbers){
            if (pred.test(list)){
                sum += list;
            }
        }
        System.out.println(sum);
    }
    
   

    
}
