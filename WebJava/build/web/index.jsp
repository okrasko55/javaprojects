<%-- 
    Document   : index
    Created on : 12.08.2017, 18:37:09
    Author     : asp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type="text/javascript">
         var getUrlParameter = function getUrlParameter(sParam) {
         var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                        sURLVariables = sPageURL.split('&'),
                        sParameterName,
                        i;

                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            };
            var nname = getUrlParameter('name');
            <%--var blog = getUrlParameter('blog'); --%>
            function popup() {
                document.getElementById('welcome').innerHTML = 'Hello,' + nname;
            }
        </script>
    </head>
    <body onload="popup()">
        <h1 id="welcome">Hello World</h1>
    </body>
</html>