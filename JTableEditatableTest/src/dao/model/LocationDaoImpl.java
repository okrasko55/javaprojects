/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.model;

import dao.model.LocationDao;
import entityClasses.Location;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;

import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import java.util.List;

public class LocationDaoImpl implements LocationDao {
 
    private static final String CREATE_SQL = "INSERT INTO HR.LOCATIONS( LOCATION_ID, STREET_ADDRESS, POSTAL_CODE, CITY, " +
                                             "STATE_PROVINCE, COUNTRY_ID) " +
                                             "VALUES (HR.LOCATIONS_SEQ.NEXTVAL, ?, ?, ?, " +
                                             "?, ?)";//версия для jdbc
    /*
    private static final String CREATE_SQL = "INSERT INTO HR.LOCATIONS( LOCATION_ID, STREET_ADDRESS, POSTAL_CODE, CITY, " +
                                             "STATE_PROVINCE, COUNTRY_ID) " +
                                             "VALUES (LOCATIONS_SEQ.NEXTVAL, :streetAddress, :postalCode, :city, " +
                                             ":stateProvince, :countryId)";//версия jdbc Spring
 */
    private static final String GET_ALL_SQL = "SELECT LOCATION_ID, STREET_ADDRESS, POSTAL_CODE, CITY, STATE_PROVINCE, COUNTRY_ID " +
                                              "FROM HR.LOCATIONS";
 /*
    private static final String GET_SQL = "SELECT LOCATION_ID, STREET_ADDRESS, POSTAL_CODE, CITY, STATE_PROVINCE, COUNTRY_ID " +
                                          "FROM HR.LOCATIONS WHERE LOCATION_ID = :locationId";
    */
    private static final String GET_SQL = "SELECT LOCATION_ID, STREET_ADDRESS, POSTAL_CODE, CITY, STATE_PROVINCE, COUNTRY_ID " +
                                          "FROM HR.LOCATIONS WHERE LOCATION_ID = ?";
    private static final String DELETE_SQL = "DELETE HR.LOCATIONS WHERE LOCATION_ID = :locationId";
    private static final String UPDATE_SQL = "UPDATE HR.LOCATIONS SET STREET_ADDRESS = ?, POSTAL_CODE=?, " +
                                            "CITY = ?, STATE_PROVINCE = ?, COUNTRY_ID = ? " +
                                            "WHERE LOCATION_ID = ?";
   /* 
    private static final String UPDATE_SQL = "UPDATE HR.LOCATIONS SET STREET_ADDRESS = :streetAddress, POSTAL_CODE=:postalCode, " +
                                            "CITY = :city, STATE_PROVINCE = :stateProvince, COUNTRY_ID = :countryId " +
                                            "WHERE LOCATION_ID = :locationId";
 */
     public static Connection  getConnection() {
        Connection connection;
        System.out.println("-------- Oracle JDBC Connection Testing ------");
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your Oracle JDBC Driver?");
            e.printStackTrace();
            return null;
        }
        System.out.println("Oracle JDBC Driver Registered!");
        try {
            
            java.util.Locale locale = java.util.Locale.getDefault();
            java.util.Locale.setDefault(java.util.Locale.ENGLISH);
            connection = DriverManager.getConnection(
                    "jdbc:oracle:thin:@localhost:1521:xe", "system",
                    "123");
            java.util.Locale.setDefault(locale);
        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
              return null;
        }
        return connection;
    }
       @Override
    public List<Location> getLocations() {
        //return jdbcTemplate.query(GET_ALL_SQL, new HashMap<String, Object>(), locationRowMapper);
        List<Location> locations = new ArrayList<Location>();
        try
        {
        Connection  connection = getConnection();            
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(GET_ALL_SQL);
        while(rs.next())
        {
                   long locationId = rs.getInt("LOCATION_ID");
                   String streetAddress = rs.getString("STREET_ADDRESS");
                   String postalCode = rs.getString("POSTAL_CODE");
                   String city = rs.getString("CITY");
                   String stateProvince = rs.getString("STATE_PROVINCE");
                   String countryId = rs.getString("COUNTRY_ID");
                   Location location = new Location(locationId,streetAddress,postalCode,city,stateProvince,countryId);
                   locations.add(location);
        }
            }
            catch(SQLException ex)
                  {
                      	System.out.println("executeQuery Failed! Check output console");
			ex.printStackTrace();
			return locations;
                  }
        return locations;
    }
    @Override
    public Location getLocation(Long locationId) {
        /*
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("locationId", locationId);
        List<Location> locations = jdbcTemplate.query(GET_SQL, params, locationRowMapper);
        return locations.isEmpty()?null:locations.get(0);
        */
        List<Location> locations = new ArrayList<Location>();
        try
        {
        Connection  connection = getConnection();            
        PreparedStatement st = connection.prepareStatement(GET_SQL);
        st.setLong(1, locationId);
       
        ResultSet rs = st.executeQuery();
        while(rs.next())
        {
                   String streetAddress = rs.getString("STREET_ADDRESS");
                   String postalCode = rs.getString("POSTAL_CODE");
                   String city = rs.getString("CITY");
                   String stateProvince = rs.getString("STATE_PROVINCE");
                   String countryId = rs.getString("COUNTRY_ID");
                   Location location = new Location(locationId,streetAddress,postalCode,city,stateProvince,countryId);
                   locations.add(location);
        }
            }
            catch(SQLException ex)
                  {
                      	System.out.println("executeQuery Failed! Check output console");
			ex.printStackTrace();
                        Location location2 = new Location(-1L,"","","","","");
			return location2;
                  }
        return locations.get(0);
    }
 @Override
    public void updateLocation (Location location) {
        /*
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("streetAddress", location.getStreetAddress())
                .addValue("postalCode", location.getPostalCode())
                .addValue("city", location.getCity())
                .addValue("stateProvince", location.getStateProvince())
                .addValue("countryId", location.getCountryId());
        jdbcTemplate.update(CREATE_SQL, params);
        */
       /*
            private static final String CREATE_SQL = "INSERT INTO HR.LOCATIONS( LOCATION_ID, STREET_ADDRESS, POSTAL_CODE, CITY, " +
                                             "STATE_PROVINCE, COUNTRY_ID) " +
                                             "VALUES (LOCATIONS_SEQ.NEXTVAL, ?, ?, ?, " +
                                             "?, ?)";
      */
        try
        {
        Connection  connection = getConnection();            
        PreparedStatement st = connection.prepareStatement(UPDATE_SQL);
        st.setString(1, location.getStreetAddress());
        st.setString(2, location.getPostalCode());
        st.setString(3, location.getCity());
        st.setString(4, location.getPostalCode());
        st.setString(5, location.getCountryId());
        st.setLong(6, location.getLocationId());
        st.executeUpdate();
            }
            catch(SQLException ex)
                  {
                      	System.out.println("executeQuery Failed! Check output console");
			ex.printStackTrace();
			return;
                  }
    }
    @Override
    public void createLocation(Location location) {
        /*
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("locationId", location.getLocationId())
                .addValue("streetAddress", location.getStreetAddress())
                .addValue("postalCode", location.getPostalCode())
                .addValue("city", location.getCity())
                .addValue("stateProvince", location.getStateProvince())
                .addValue("countryId", location.getCountryId());
        jdbcTemplate.update(UPDATE_SQL, params);
        */
         try
        {
        Connection  connection = getConnection();            
        PreparedStatement st = connection.prepareStatement(CREATE_SQL);
        st.setString(1, location.getStreetAddress());
        st.setString(2, location.getPostalCode());
        st.setString(3, location.getCity());
        st.setString(4, location.getPostalCode());
        st.setString(5, location.getCountryId());
        st.executeUpdate();
            }
            catch(SQLException ex)
                  {
                      	System.out.println("executeQuery Failed! Check output console");
			ex.printStackTrace();
			return;
                  }
    }
    @Override
    public void deleteLocation(Long locationId) {
        //jdbcTemplate.update(DELETE_SQL, new MapSqlParameterSource("locationId",locationId));
    }
}