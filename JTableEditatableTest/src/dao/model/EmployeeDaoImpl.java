/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.model;

import static dao.model.LocationDaoImpl.getConnection;
import entityClasses.Employee;
import entityClasses.Location;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author asp
 */
public class EmployeeDaoImpl implements EmployeeDao{

    private static final String GET_ALL_SQL = "SELECT LAST_NAME, SALARY, EMPLOYEE_ID " +
                                              "FROM HR.EMPLOYEES";
    
    private static final String GET_SQL = "SELECT LAST_NAME, SALARY, EMPLOYEE_ID " +
                                            "FROM HR.EMPLOYEES WHERE EMPLOYEE_ID = ?";
    
    private static final String UPDATE_SQL = "UPDATE HR.EMPLOYEES SET LAST_NAME = ?, SALARY = ? " +
                                            "WHERE EMPLOYEE_ID = ?";
    
    public static Connection  getConnection() {
        Connection connection;
        System.out.println("-------- Oracle JDBC Connection Testing ------");
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your Oracle JDBC Driver?");
            e.printStackTrace();
            return null;
        }
        System.out.println("Oracle JDBC Driver Registered!");
        try {
            
            java.util.Locale locale = java.util.Locale.getDefault();
            java.util.Locale.setDefault(java.util.Locale.ENGLISH);
            connection = DriverManager.getConnection(
                    "jdbc:oracle:thin:@localhost:1521:xe", "system",
                    "123");
            java.util.Locale.setDefault(locale);
        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
              return null;
        }
        return connection;
    }
    @Override
    public void createEmployee(Employee employee) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Employee> getEmployees() {
        List<Employee> employees = new ArrayList<Employee>();
        try
        {
        Connection  connection = getConnection();            
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(GET_ALL_SQL);
        while(rs.next())
        {
                   int age = rs.getInt("EMPLOYEE_ID");
                   int salary = rs.getInt("SALARY");
                   String name = rs.getString("LAST_NAME");
                   Employee employee = new Employee(name, salary, age);
                   employees.add(employee);
        }
            }
            catch(SQLException ex)
                  {
                      	System.out.println("executeQuery Failed! Check output console");
			ex.printStackTrace();
			return employees;
                  }
        return employees;
    }

    @Override
    public Employee getEmployee(int employeeId) {
        List<Employee> employees = new ArrayList<Employee>();
//        Employee employee = null;
        try{
            Connection  connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(GET_SQL);
            preparedStatement.setInt(1, employeeId);
            ResultSet rs = preparedStatement.executeQuery();
            while(rs.next()){
                int age = rs.getInt("EMPLOYEE_ID");
                int salary = rs.getInt("SALARY");
                String name = rs.getString("LAST_NAME");
//                employee = new Employee(name, salary, age);
                Employee employee = new Employee(name, salary, age);
                employees.add(employee);
            }
        }catch(SQLException ex){
            System.out.println("executeQuery Failed! Check output console");
            ex.printStackTrace();
            Employee employee2 = new Employee("", 0, -1);
            return employee2;
        }
        return employees.get(0);
//        return employee;
    }

    @Override
    public void updateEmployee(Employee employee) {
        try {
            Connection connection = getConnection();
            PreparedStatement st = connection.prepareStatement(UPDATE_SQL);
            st.setString(1, employee.getName());
            st.setInt(2, employee.getSalary());
            st.setInt(3, employee.getAge());
            st.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("executeQuery Failed! Check output console");
            ex.printStackTrace();
            return;
        }
    }

    @Override
    public void deleteLocation(Long employeeId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
