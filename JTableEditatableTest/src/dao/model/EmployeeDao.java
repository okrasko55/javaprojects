/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.model;

import entityClasses.Employee;
import entityClasses.Location;
import java.util.List;

/**
 *
 * @author asp
 */
public interface EmployeeDao {
    public void createEmployee(Employee employee);
    public List<Employee> getEmployees();
    public Employee getEmployee(int employeeId);
    public void updateEmployee(Employee employee);
    public void deleteLocation(Long employeeId);
}
