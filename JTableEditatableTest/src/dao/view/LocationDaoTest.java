/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.view;

import dao.model.LocationDao;
import entityClasses.Location;
import java.util.List;

public class LocationDaoTest {
    private LocationDao locationDao;
    public LocationDaoTest(LocationDao locationDao)
    {
        this.locationDao = locationDao;
    }
    public void testSelectAllLocations(){
    List<Location> locations = locationDao.getLocations();
    System.out.println("testSelectAllLocations(): ");
    System.out.println(locations);
    }
    public void testSelectOneLocation(){
      Location location = locationDao.getLocation(1000L);
        System.out.println("getLocation(): " + location);
    }
    public void testDeleteLocation(){
        locationDao.deleteLocation(1000L);
    }
    public void testInsertLocation(){
        int beforeInsert = locationDao.getLocations().size();
        Location location = new Location(1000L, "test", "1111", "aaa", "aaa", "IT");
        locationDao.createLocation(location);
        System.out.println("createLocation(): " + (locationDao.getLocations().size() - beforeInsert)); 
    }
    public void testUpdateLocation(){
        
    }
}

