/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.view;

import dao.model.EmployeeDao;
import dao.model.EmployeeDaoImpl;
import entityClasses.Employee;
import entityClasses.Location;
import java.util.List;

/**
 *
 * @author asp
 */
public class EmployeeDaoTest {
    private EmployeeDao employeeDao;
    
    public EmployeeDaoTest(EmployeeDao employeeDao){
        this.employeeDao = employeeDao;
    }
    
    public void testSelectAllEmployees(){
        List<Employee> employees = employeeDao.getEmployees();
        System.out.println("test getEmployees(): ");
        System.out.println(employees);
    }
    
    public void testSelectOneEmployee(int employeeId){
        Employee employee = employeeDao.getEmployee(employeeId);
        System.out.println("test getEmployee(): " + employee);
    }
    
    public void testUpdateEmployees(String name, int salary, int employeeId){
        Employee employee = new Employee(name, salary, employeeId);
        employeeDao.updateEmployee(employee);
        System.out.println("test updateEmployee(): " + employeeDao.getEmployee(employeeId));
    }
     
}
