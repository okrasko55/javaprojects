package Controller;

import Model.DAO.*;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

public class CountryTableModel extends AbstractTableModel {

    //
    DaoCountryInterface countryDao = new DaoCountry();
    //
    private String[] columnNames = {"Id.", "Название Страны", "Region"};
    private List<Country> listCountry = new ArrayList<>();

    public CountryTableModel(List<Country> listPerson) {
        this.listCountry.addAll(listPerson);
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public Class getColumnClass(int column) {
        return getValueAt(0, column).getClass();
    }

    @Override
    public int getRowCount() {
        return listCountry.size();
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        Country country = listCountry.get(rowIndex);

        switch (columnIndex) {
            case 0:
                country.setCountry_Id((String) value);
                break;
            case 1:
                country.setCountry_name((String) value);
                break;
            case 2:
                country.setRegion((Region) value);
                break;
        }
        countryDao.update(country);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object returnValue = null;
        Country country = listCountry.get(rowIndex);

        switch (columnIndex) {
            case 0:
                returnValue = country.getCountry_Id();
                break;
            case 1:
                returnValue = country.getCountry_name();
                break;
            case 2:
                returnValue = country.getRegion();
                break;
        }

        return returnValue;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex > 0;
    }

    public void addRow(Country newCountry) {
        countryDao.create(newCountry);
    }

    public void removeRow(int selectedRow) {
        Country country = listCountry.get(selectedRow);
        countryDao.delete(country.getCountry_Id());
    }

    public Country getCurrentRow(int selectedRow) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
