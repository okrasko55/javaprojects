/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import entityClasses.Location;
import Model.DAO.*;


public class LocationTableModel extends AbstractTableModel
{
    private final List<Location> locationList;
    private final LocationDao locationDao;
    private final String[] columnNames = new String[] {
            "Id", "Улица", "Почтовый индекс", "City","State Province","Country Id"
    };
    private final Class[] columnClass = new Class[] {
        Long.class, String.class, String.class, String.class, String.class, String.class
    };
    public LocationTableModel()
    {
        locationDao = new LocationDaoImpl();
        this.locationList = createLocationsList();
    }
     private List<Location> createLocationsList() {
        
        List<Location> locations = locationDao.getLocations();
        return locations;
    }
    
     @Override
    public String getColumnName(int column)
    {
        return columnNames[column];
    }
    @Override
    public Class<?> getColumnClass(int columnIndex)
    {
        return columnClass[columnIndex];
    }
    @Override
    public int getColumnCount()
    {
        return columnNames.length;
    }
    @Override
    public int getRowCount()
    {
        return locationList.size();
    }
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
        if (columnIndex!=0)
            return true;
        else
            return false;
    }
    @Override
    public Object getValueAt(int rowIndex, int columnIndex)
    {
        Location row = locationList.get(rowIndex);
        if(0 == columnIndex) {
            return row.getLocationId();
        }
        else if(1 == columnIndex) {
            return row.getStreetAddress();
        }
        else if(2 == columnIndex) {
            return row.getPostalCode();
        }
        else if(3 == columnIndex) {
            return row.getCity();
        }else if(4 == columnIndex) {
            return row.getStateProvince();
        }else if(5 == columnIndex) {
            return row.getCountryId();
        }
        
        return null;
    }
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex)
    {
        Location row = locationList.get(rowIndex);
        if(0 == columnIndex) {
            row.setLocationId((Long) aValue);
        }
        else if(1 == columnIndex) {
            row.setStreetAddress((String) aValue);
        }
        else if(2 == columnIndex) {
            row.setPostalCode((String) aValue);
        }
        else if(3 == columnIndex) {
            row.setCity((String) aValue);
        }
        else if(4 == columnIndex) {
            row.setStateProvince((String) aValue);
        }
        else if(5 == columnIndex) {
            row.setCountryId((String) aValue);
        }
        locationDao.updateLocation(row);
    }   
}
