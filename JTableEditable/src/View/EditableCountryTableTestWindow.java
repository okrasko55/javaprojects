/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Model.DAO.*;
import Controller.CountryTableModel;
import Controller.TableMouseListener;

public class EditableCountryTableTestWindow extends JFrame implements ActionListener {

    private JTable table = new JTable();
    private CountryTableModel tableModel;
    private JMenuItem menuItemAdd;
    private JMenuItem menuItemRemove;
    private JMenuItem menuItemEditCurrent;

    public EditableCountryTableTestWindow() {
        super("JComboBox Cell Editor for JTable Editable Test");
        List<Country> listCountry = new ArrayList<>();
        DaoCountryInterface countryDao = new DaoCountry();

//        listCountry = countryDao.select();
        listCountry.add(countryDao.selectOne("AR"));
        listCountry.add(countryDao.selectOne("AU"));
        listCountry.add(countryDao.selectOne("BE"));
        listCountry.add(countryDao.selectOne("DK"));
        listCountry.add(countryDao.selectOne("UA"));

        List<Region> listRegion = new ArrayList<>();
        listRegion.add(new Region(1, "Europe"));
        listRegion.add(new Region(2, "Americas"));
        listRegion.add(new Region(3, "Asia"));
        listRegion.add(new Region(4, "Middle East and Africa"));
        tableModel = new CountryTableModel(listCountry);
        //

        table.setModel(tableModel);

        table.setDefaultRenderer(Region.class, new View.Helper.RegionCellRenderer());
        table.setDefaultEditor(Region.class, new View.Helper.RegionCellEditor(listRegion));

        table.setRowHeight(25);

        JPopupMenu popupMenu = new JPopupMenu();
        menuItemAdd = new JMenuItem("Add New Row");
        menuItemRemove = new JMenuItem("Remove Current Row");
        popupMenu.add(menuItemAdd);
        popupMenu.add(menuItemRemove);
        //popupMenu.add(menuItemEditCurrent);
        // sets the popup menu for the table
        menuItemAdd.addActionListener(this);
        menuItemRemove.addActionListener(this);
        //menuItemEditCurrent.addActionListener(this);
        table.setComponentPopupMenu(popupMenu);
        table.addMouseListener(new TableMouseListener(table));

        JScrollPane scrollpane = new JScrollPane(table);
        scrollpane.setPreferredSize(new Dimension(400, 200));
        add(scrollpane, BorderLayout.CENTER);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);

        //
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        JMenuItem menu = (JMenuItem) event.getSource();
        if (menu == menuItemAdd) {
            addNewRow();
        } else if (menu == menuItemRemove) {
            removeCurrentRow();
        } else if (menu == menuItemEditCurrent) {
            //editCurrentRow();
        }

    }

    private void addNewRow() {
        Country newCountry = new Country("", "", 0);
        CountryWindow newCountryWindow = new CountryWindow(this, newCountry);
        newCountryWindow.setVisible(true);
        newCountry = newCountryWindow.country;
        if ((newCountry != null) && (newCountry.getCountry_Id() != null) && (newCountry.getCountry_Id().length() > 0)) {
            tableModel.addRow(newCountry);
        }
    }

    private void removeCurrentRow() {

        int selectedRow = table.getSelectedRow();
        tableModel.removeRow(selectedRow);

    }

}
