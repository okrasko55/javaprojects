/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.DAO.Country;
import Model.DAO.Region;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class CountryWindow extends JDialog {

    private JButton buttonCreate = new JButton("Create");
    private JButton buttonCancel = new JButton("Cancel");
    private JTextField fieldCountryId = new JTextField(25);
    private JTextField fieldCountryName = new JTextField(25);
    private Region selectedRegion;
    public Country country;

    public CountryWindow(Frame parent, Country c) {
        super(parent, "Add New Country", true);
        this.country = c;
        setLayout(new FlowLayout(FlowLayout.LEFT, 10, 10));
        //fieldCountryId.setSize( new Dimension(100, 25));
        //fieldCountryName.setSize( new Dimension(100, 25));
        List<Region> listRegion = new ArrayList<>();
        listRegion.add(new Region(1, "Europe"));
        listRegion.add(new Region(2, "Americas"));
        listRegion.add(new Region(3, "Asia"));
        listRegion.add(new Region(4, "Middle East and Africa"));
        // create a combo box with items specified in the String array:
        JComboBox<Region> comboRegion = new JComboBox<Region>();

        for (Region aRegion : listRegion) {
            comboRegion.addItem(aRegion);
        }
        // customize some appearance:
        comboRegion.setForeground(Color.BLUE);
        comboRegion.setFont(new Font("Arial", Font.BOLD, 14));
        comboRegion.setMaximumRowCount(10);
        // make the combo box editable so we can add new item when needed
        comboRegion.setEditable(false);
        // add an event listener for the combo box
        comboRegion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                JComboBox<Region> combo = (JComboBox<Region>) event.getSource();
                selectedRegion = (Region) combo.getSelectedItem();
                country.setRegion(selectedRegion);
                country.setCountry_name(fieldCountryName.getText());
                country.setCountry_Id(fieldCountryId.getText());
            }
        });
        //
        // add event listener for the button Select 
        buttonCreate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                setVisible(false);
            }
        });
        // add event listener for the button Remove
        buttonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                setVisible(false);
            }
        });
        // add components to this frame
        add(fieldCountryId);
        add(fieldCountryName);
        add(comboRegion);
        add(buttonCreate);
        add(buttonCancel);

        pack();
        setResizable(false);
        setLocationRelativeTo(parent);
    }
}
