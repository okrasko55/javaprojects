package View;

import Controller.LocationTableModel;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import entityClasses.Location;
import Model.DAO.*;

public class EditableLocationTableTestWindow extends JFrame {

    public EditableLocationTableTestWindow() {
        //create the model
        LocationTableModel model = new LocationTableModel();
        //create the table
        JTable table = new JTable(model);
        //set the renderer if we have

        //add the table to the frame
        this.add(new JScrollPane(table));

        this.setTitle("Editable Table HR.Location");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
    }

}
