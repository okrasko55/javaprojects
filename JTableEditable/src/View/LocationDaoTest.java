package View;

import entityClasses.Location;
import java.util.List;
import Model.DAO.*;
import com.sun.org.apache.xpath.internal.operations.Bool;

public class LocationDaoTest {
    private LocationDao locationDao;
    public LocationDaoTest(LocationDao locationDao)
    {
        this.locationDao = locationDao;
    }
    public void testSelectAllLocations(){
    List<Location> locations = locationDao.getLocations();
    System.out.println("testSelectAllLocations(): ");
    System.out.println(locations);
    }
    public void testSelectOneLocation(){
      Location location = locationDao.getLocation(1000L);
      System.out.print("testSelectOneLocation(): "
              +location);
    }
    public void testDeleteLocation(){
        locationDao.deleteLocation(1000L);
    }
    public void testInsertLocation(){
    int sizeBeforeInsert = locationDao.getLocations().size();
    Location location = new Location(1000L,
                                     "test",
                                      "11111",
                                       "athens",
                                      "athens",
                                      "IT");
        locationDao.createLocation(location);
    int sizeAterInsert = locationDao.getLocations().size();
    Boolean b = ((sizeAterInsert - sizeBeforeInsert)==1);
    }
    public void testUpdateLocation(){
        Location newLocation = new Location(1000L,"test","11111","athens","athens","IT");
        locationDao.updateLocation(newLocation);
        Location changedLocation = locationDao.getLocation(1000L);
    }
}
