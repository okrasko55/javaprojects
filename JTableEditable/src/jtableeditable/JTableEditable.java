package jtableeditable;

import Model.DAO.LocationDao;
import Model.DAO.LocationDaoImpl;
import View.LocationDaoTest;
import View.EditableLocationTableTestWindow;
import View.EditableCountryTableTestWindow;
import javax.swing.SwingUtilities;

public class JTableEditable {
    public static void main(String[] args) {
      /*  
      LocationDao locationDao = new LocationDaoImpl();
        LocationDaoTest locationDaoTest = 
                new LocationDaoTest(locationDao);
        locationDaoTest.testSelectAllLocations();
        locationDaoTest.testSelectOneLocation();
        locationDaoTest.testInsertLocation();
        */
      SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
//                new EditableLocationTableTestWindow();
                new EditableCountryTableTestWindow();
            }
        });
    }
}
