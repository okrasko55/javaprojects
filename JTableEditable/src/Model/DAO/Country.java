package Model.DAO;

public class Country implements SqlTextInterface {

    public static final String CREATE_SQL = "INSERT INTO HR.COUNTRIES (COUNTRY_ID,COUNTRY_NAME,REGION_ID) "
            + "VALUES (?,?,?)";
    public static final String GET_ALL_SQL = "SELECT CTR.COUNTRY_ID, CTR.COUNTRY_NAME, CTR.REGION_ID, REG.REGION_NAME FROM HR.COUNTRIES CTR LEFT JOIN HR.REGIONS REG ON CTR.REGION_ID=REG.REGION_ID";
    public static final String GET_SQL = "SELECT CTR.COUNTRY_ID, CTR.COUNTRY_NAME, CTR.REGION_ID, REG.REGION_NAME FROM HR.COUNTRIES CTR LEFT JOIN HR.REGIONS REG ON CTR.REGION_ID=REG.REGION_ID WHERE CTR.COUNTRY_ID = ?";
    public static final String DELETE_SQL = "DELETE HR.COUNTRIES WHERE COUNTRY_ID = ?";
    public static final String UPDATE_SQL = "UPDATE HR.COUNTRIES SET COUNTRY_NAME = ?, REGION_ID=?, COUNTRY_ID = ? "
            + "WHERE COUNTRY_ID = ?";
    
    private String country_Id;
    private String country_name;
    private int region_id;
    private Region region;

    public Country(String countryId, String countryName, int regionId) {
        this.country_Id = countryId;
        this.country_name = countryName;
        this.region_id = regionId;
    }

    public Country(String countryId, String countryName, Region region) {
        this.country_Id = countryId;
        this.country_name = countryName;
        this.region = region;
        this.region_id = region.getRegion_id();
    }

    public String getCountry_Id() {
        return country_Id;
    }

    public void setCountry_Id(String country_Id) {
        this.country_Id = country_Id;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public int getRegion_id() {
        return region_id;
    }

    public void setRegion_id(int region_id) {
        this.region_id = region_id;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
        this.setRegion_id(region.getRegion_id());
    }

    @Override
    public String getCREATE_SQL() {
        return CREATE_SQL;
    }

    @Override
    public String GET_ALL_SQL() {
        return GET_ALL_SQL;
    }

    @Override
    public String GET_SQL() {
        return GET_SQL;
    }

    @Override
    public String DELETE_SQL() {
        return DELETE_SQL;
    }

    @Override
    public String UPDATE_SQL() {
        return UPDATE_SQL;
    }
}
