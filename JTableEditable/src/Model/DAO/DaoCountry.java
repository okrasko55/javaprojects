package Model.DAO;

import Model.DAO.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DaoCountry implements DaoCountryInterface {

    public static Connection getConnection() {
        Connection connection;
        System.out.println("-------- Oracle JDBC Connection Testing ------");
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your Oracle JDBC Driver?");
            e.printStackTrace();
            return null;
        }
        System.out.println("Oracle JDBC Driver Registered!");
        try {

            java.util.Locale locale = java.util.Locale.getDefault();
            java.util.Locale.setDefault(java.util.Locale.ENGLISH);
            connection = DriverManager.getConnection(
                    "jdbc:oracle:thin:@localhost:1521:xe", "system",
                    "123");
            java.util.Locale.setDefault(locale);
        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return null;
        }
        return connection;
    }
    @Override
    public void create(Country country) {
        try {
            Connection connection = getConnection();
            //COUNTRY_ID,COUNTRY_NAME,REGION_ID
            PreparedStatement st = connection.prepareStatement(country.getCREATE_SQL());
            String country_ID = country.getCountry_Id();
            String country_Name = country.getCountry_name();
            int region_id = country.getRegion_id();
            st.setString(1, country_ID);
            st.setString(2, country_Name);
            st.setInt(3, region_id);
            st.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("executeQuery Failed! Check output console");
            ex.printStackTrace();
            return;
        }
    }
    @Override
    public List<Country> select() {
        List<Country> countries = new ArrayList<Country>();
        //COUNTRY_ID,COUNTRY_NAME,REGION_ID,REGION_NAME
        try {
            Connection connection = getConnection();
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(Country.GET_ALL_SQL);
            while (rs.next()) {
                int regionId = rs.getInt("REGION_ID");
                String countryName = rs.getString("COUNTRY_NAME");
                String regionName = rs.getString("REGION_NAME");
                String countryId = rs.getString("COUNTRY_ID");
                Region region = new Region(regionId, regionName);
                Country country = new Country(countryId, countryName, region);
                countries.add(country);
            }
        } catch (SQLException ex) {
            System.out.println("executeQuery Failed! Check output console");
            ex.printStackTrace();
            return countries;
        }
        return countries;
    }
    @Override
    public Country selectOne(String countryId) {
        List<Country> countries = new ArrayList<Country>();
        try {
            Connection connection = getConnection();
            PreparedStatement st = connection.prepareStatement(Country.GET_SQL);
            st.setString(1, countryId);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int regionId = rs.getInt("REGION_ID");
                String countryName = rs.getString("COUNTRY_NAME");
                String regionName = rs.getString("REGION_NAME");
                Region region = new Region(regionId, regionName);
                Country country = new Country(countryId, countryName, region);
                countries.add(country);
            }
        } catch (SQLException ex) {
            System.out.println("executeQuery Failed! Check output console");
            ex.printStackTrace();
            Region region2 = new Region(0, "");
            Country country2 = new Country("", "", region2);
            return country2;
        }
        if (countries.isEmpty()) {
            Region region2 = new Region(0, "");
            Country country2 = new Country("", "", region2);
            return country2;
        } else {
            return countries.get(0);
        }
    }
    @Override
    public void update(Country country) {
        try {
            Connection connection = getConnection();
            PreparedStatement st = connection.prepareStatement(country.UPDATE_SQL());
            st.setString(1, country.getCountry_name());
            st.setInt(2, country.getRegion_id());
            st.setString(3, country.getCountry_Id());
            st.setString(4, country.getCountry_Id());
            st.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("executeQuery Failed! Check output console");
            ex.printStackTrace();
            return;
        }
    }
    @Override
    public void delete(String countryId) {
        try {
            Connection connection = getConnection();
            PreparedStatement st = connection.prepareStatement(Country.DELETE_SQL);
            st.setString(1, countryId);
            st.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("executeQuery Failed! Check output console");
            ex.printStackTrace();
            return;
        }
    }
}

