package Model.DAO;

import java.util.List;


public interface DaoCountryInterface {
    public void create(Country location);
    public List<Country> select();
    public Country selectOne(String countryId);
    public void update(Country location);
    public void delete(String countryId);
}
