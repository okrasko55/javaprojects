/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.DAO;

public interface SqlTextInterface {
    public String getCREATE_SQL();
    public String GET_ALL_SQL();
    public String GET_SQL();
    public String DELETE_SQL();
    public String UPDATE_SQL();
}