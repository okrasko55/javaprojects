package Model.DAO;

public class Region implements SqlTextInterface{
    public static final String CREATE_SQL = "INSERT INTO HR.REGIONS (REGION_ID,REGION_NAME) " +
                                             "VALUES (HR.LOCATIONS_SEQ.NEXTVAL,?)";
    public static final String GET_ALL_SQL = "SELECT REGION_ID, REGION_NAME FROM HR.REGIONS";
    public static final String GET_SQL = "SELECT REGION_ID, REGION_NAME FROM HR.REGIONS WHERE REGION_ID = ?";
    public static final String DELETE_SQL = "DELETE HR.REGIONS WHERE REGION_ID = ?";
    public static final String UPDATE_SQL = "UPDATE HR.REGIONS SET REGION_NAME = ?, REGION_ID=? " +
                                            "WHERE REGION_ID = ?";
    private int region_id;
    private String region_name;
    
    public Region(int regionId, String regionName)
    {
        this.region_id = regionId;
        this.region_name = regionName;
    }
    public int getRegion_id() {
        return region_id;
    }
    public void setRegion_id(int region_id) {
        this.region_id = region_id;
    }
    public String getRegion_name() {
        return region_name;
    }
    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }
    	public String toString() {
		return this.region_name;
	}
        @Override
    public String getCREATE_SQL() {
       return CREATE_SQL;
    }
    @Override
    public String GET_ALL_SQL() {
        return GET_ALL_SQL;
    }
    @Override
    public String GET_SQL() {
        return GET_SQL;
    }
    @Override
    public String DELETE_SQL() {
        return DELETE_SQL;
    }
    @Override
    public String UPDATE_SQL() {
        return UPDATE_SQL;
    }
}
