/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myswingapplication;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingConstants;

/**
 *
 * @author asp
 */
class MyWindow extends JFrame implements ActionListener{
    
    private JPanel myWorkSpace;
    private BorderLayout lmBoredrLayout;
    private JButton btnAction;
    private JLabel lbMyMessage; 

    public MyWindow(){
            try {
                initComponents();
            } catch (Exception ex) {

            }
    }


    private void initComponents()throws Exception{
        this.setSize(400, 300);
        this.setTitle("My first swing test app");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        myWorkSpace = (JPanel) this.getContentPane();
        lmBoredrLayout = new BorderLayout();
        myWorkSpace.setLayout(lmBoredrLayout);

        lbMyMessage = new JLabel();
        lbMyMessage.setText("0");
        lbMyMessage.setToolTipText("");
        lbMyMessage.setHorizontalAlignment(SwingConstants.CENTER);

        btnAction = new JButton();
        btnAction.setText("Push me");
        btnAction.setToolTipText("<html><h1>push me <br> push me</></>");
        
        btnAction.addActionListener(this);

        myWorkSpace.add(btnAction, BorderLayout.SOUTH);
        myWorkSpace.add(lbMyMessage, BorderLayout.CENTER);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        String currentValue = lbMyMessage.getText();
        int n = Integer.parseInt(currentValue);
        lbMyMessage.setText(String.valueOf(++n));    
    }
}
