/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package swinghellotest;

import javax.swing.*;
/**
 *
 * @author asp
 */
public class SwingHelloTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JFrame myWindow = new JFrame("HelloSwing");
        JLabel myMessage = new JLabel("Hello world!");
        
        JPanel myWorkSpace = (JPanel) myWindow.getContentPane();
        myWorkSpace.add(myMessage);
        myWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myWindow.pack();
        myWindow.setVisible(true);
    }
    
}
