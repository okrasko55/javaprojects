/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao.interfaces;

import entityClasses.Location;

/**
 *
 * @author Oleg K
 */
public interface LocationDaoInterface extends BaseDaoInterface<Location>{
    public void createLocation(Location location);
    public void updateLocation(Location location, Long id);
    
}
