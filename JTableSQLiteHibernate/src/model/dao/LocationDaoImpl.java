/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import entityClasses.Location;
import model.dao.interfaces.LocationDaoInterface;
import org.hibernate.Query;

/**
 *
 * @author Oleg K
 */
public class LocationDaoImpl extends BaseDaoImpl<Location> implements LocationDaoInterface{

    public LocationDaoImpl() {
        super(Location.class);
    }

    @Override
    public void createLocation(Location location) {
        openSession();
        String hql = "insert into Location (streetAddress, postalCode, city, stateProvince, countryId) select streetAddress, postalCode, city, stateProvince, countryId from " + location;
        Query query = session.createQuery(hql);
        query.executeUpdate();
    }

    @Override
    public void updateLocation(Location location, Long id) {
        openSession();
//        String hql = "update Location set streetAddress = " + location.getStreetAddress() + 
//                    ", postalCode = " + location.getPostalCode() + 
//                    ", city = " + location.getCity() + 
//                    ", stateProvince = " + location.getStateProvince() + 
//                    ", countryId = " + location.getCountryId() + 
//                    " where locationId = " + location.getLocationId();
//        Query query = session.createQuery(hql);
//        int result =  query.executeUpdate();
        Location updatedLocation = (Location) session.load(Location.class, id);
        updatedLocation.setCity(location.getCity());
        updatedLocation.setCountryId(location.getCountryId());
        updatedLocation.setPostalCode(location.getPostalCode());
        updatedLocation.setStateProvince(location.getStateProvince());
        updatedLocation.setStreetAddress(location.getStreetAddress());
        
        closeSession();
    }
    
}
