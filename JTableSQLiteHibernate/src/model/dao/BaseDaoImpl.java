/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import entityClasses.Location;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import model.dao.interfaces.BaseDaoInterface;

/**
 *
 * @author Oleg K
 */
public class BaseDaoImpl<Model> implements BaseDaoInterface<Model>{

    private final Class<Model> clazz;
    protected Session session;
    protected ServiceRegistry serviceRegistry;
    
    public BaseDaoImpl(Class<Model> clazz){
        this.clazz = clazz;
    }
    
    void openSession(){
        Configuration configuration = new Configuration().configure();
        serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties()).build();

        // builds a session factory from the service registry
        SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        // obtains the session
        session = sessionFactory.openSession();
        session.beginTransaction();
    }
    
    void closeSession(){
        session.getTransaction().commit();
        session.close();
        StandardServiceRegistryBuilder.destroy(serviceRegistry);
    }


    @Override
    public List<Model> getAll() {
        openSession();
//        String className = clazz.getSimpleName();
//        String hql = "from " + className;
//        Query query = session.createQuery(hql);
        List<Model> listLocations = session.createCriteria(clazz).list();
        closeSession();
        return listLocations;
    }

    @Override
    public Model getById(Long id) {
        openSession();
//        String className = clazz.getSimpleName();
//        String hql = "from " + className + " where id = " + id;
//        Query query = session.createQuery(hql);
        Model location = (Model) session.load(clazz, id);
//        closeSession();
        return location;
    }

    @Override
    public void delete(Model model) {
        openSession();
//        String className = clazz.getSimpleName();
//        String hql = "delete " + className + " where id = " + id;
//        Query query = session.createQuery(hql);
        session.delete(model);
        closeSession();
    }

    @Override
    public void add(Model model) {
        openSession();
        session.save(model);
        closeSession();
    }

    @Override
    public void update(Model model, Long id) {
        openSession();
        session.update(model);
        session.flush();
        closeSession();
    }
    
}
