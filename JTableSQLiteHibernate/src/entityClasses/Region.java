/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entityClasses;

import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Oleg K
 */
@Entity
@Table(name = "Regions")
public class Region {

    //variables
    private int regionId;
    private String regionName;
    private Set<Country> countries;

    //constructors
    public Region() {
    }

    public Region(int regionId, String regionName) {
        this.regionId = regionId;
        this.regionName = regionName;
    }

    //getters and setters
    @Id
    @Column(name = "region_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getRegionId() {
        return regionId;
    }

    public void setRegionId(int region_id) {
        this.regionId = region_id;
    }

    @Column(name = "region_name")
    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String region_name) {
        this.regionName = region_name;
    }

    @Override
    public String toString() {
        return this.regionName;
    }

    @OneToMany(mappedBy = "region", cascade = CascadeType.ALL)
    public Set<Country> getCountries() {
        return countries;
    }

    public void setCountries(Set<Country> countries) {
        this.countries = countries;
    }
}
