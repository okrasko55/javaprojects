/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entityClasses;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Oleg K
 */
@Entity
@Table(name = "Locations")
public class Location implements Comparable<Location>, Serializable {

    @Id
    @Column(name = "location_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long locationId;
    @Column(name = "street_address")
    private String streetAddress;
    @Column(name = "postal_code")
    private String postalCode;
    @Column(name = "city")
    private String city;
    @Column(name = "state_province")
    private String stateProvince;
    @Column(name = "country_id")
    private String countryId;
    
    public Location() {}

    public Location(Long locationId, String streetAddress, String postalCode, String city, String stateProvince, String countryId) {
        this.locationId = locationId;
        this.streetAddress = streetAddress;
        this.postalCode = postalCode;
        this.city = city;
        this.stateProvince = stateProvince;
        this.countryId = countryId;
    }

    @Override
    public String toString() {
        return String.format("(%d,%s,%s,%s,%s,%s)", locationId, streetAddress, postalCode, city, stateProvince, countryId);
    }

    
    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStateProvince() {
        return stateProvince;
    }

    public void setStateProvince(String stateProvince) {
        this.stateProvince = stateProvince;
    }

   
    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    @Override
    public boolean equals(Object o) {
        //сравниваем с null
        //совместимость типа
        //сравниваем ссылки - не сам ли объект пришел
        //логика различия: сравниваем поля
        boolean result = false;
        if (o != null) {
            if (o instanceof Location) {
                if (o != this) {
                    Location other = (Location) o;
                    boolean fid = (this.getLocationId() == other.getLocationId()) ? true : false;
                    result = fid;
                }
            }
        }
        return result; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int compareTo(Location location) {
        return (int) (location.locationId - this.locationId);
    }
}
