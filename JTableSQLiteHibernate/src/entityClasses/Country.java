/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entityClasses;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Oleg K
 */
@Entity
@Table(name = "Countries")
public class Country {
    //variables
    private Long countryId;
    private String countryName;
    private Region region;

    //constructors
    public Country(){}
    public Country(Long countryId, String countryName, int regionId) {
        this.countryId = countryId;
        this.countryName = countryName;
    }
    
    public Country(Long countryId, String countryName, Region region) {
        this.countryId = countryId;
        this.countryName = countryName;
        this.region = region;
    }



    //getters and setters
    @Id
    @Column(name = "country_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long country_Id) {
        this.countryId = country_Id;
    }

    @Column(name = "country_name")
    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String country_name) {
        this.countryName = country_name;
    }

    @ManyToOne
    @JoinColumn(name = "region_id")
    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
//        this.setRegionId(region.getRegionId());
    }
}
