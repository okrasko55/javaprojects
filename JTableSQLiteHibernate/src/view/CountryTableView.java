/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.CountryTableModel;
import controller.TableMouseListener;
import entityClasses.Country;
import entityClasses.Region;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import model.dao.BaseDaoImpl;
import model.dao.interfaces.BaseDaoInterface;

/**
 *
 * @author Oleg K
 */
public class CountryTableView extends JFrame implements ActionListener{
    
    //variables
    private JTable table = new JTable();
    private CountryTableModel tableModel;
    private JMenuItem menuItemAdd;
    private JMenuItem menuItemRemove;
    
    //constructors
    public CountryTableView(){
        super("JComboBox Cell Editor for JTable Editable Test");
        BaseDaoInterface countryDao = new BaseDaoImpl(Country.class);
        List<Country> listCountry = countryDao.getAll();
        tableModel = new CountryTableModel(listCountry);
        table.setModel(tableModel);

        //setting combobox
        BaseDaoInterface<Region> regionDao = new BaseDaoImpl<>(Region.class);
        List<Region> regions = regionDao.getAll();
        table.setDefaultRenderer(Region.class, new view.helper.RegionCellRenderer());
        table.setDefaultEditor(Region.class, new view.helper.RegionCellEditor(regions));

        //setting popup menu
        JPopupMenu popupMenu = new JPopupMenu();
        menuItemAdd = new JMenuItem("Add New Row");
        menuItemRemove = new JMenuItem("Remove Current Row");
        popupMenu.add(menuItemAdd);
        popupMenu.add(menuItemRemove);
        menuItemAdd.addActionListener(this);
        menuItemRemove.addActionListener(this);
        table.setComponentPopupMenu(popupMenu);
        table.addMouseListener(new TableMouseListener(table));
        
        //displaying
        JScrollPane scrollpane = new JScrollPane(table);
        scrollpane.setPreferredSize(new Dimension(400, 200));
        add(scrollpane, BorderLayout.CENTER);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }
    
    private void addNewRow() {
        Country newCountry = new Country(0L, "", 0);
        CountryDialogView dialogView = new CountryDialogView(this, newCountry);
        dialogView.setVisible(true);
        newCountry = dialogView.country;
        if ((newCountry != null) && (newCountry.getCountryId() != 0)) {
            tableModel.addRow(newCountry);
        }
    }

    private void removeCurrentRow() {
        int selectedRow = table.getSelectedRow();
        tableModel.removeRow(selectedRow);
    }

      @Override
    public void actionPerformed(ActionEvent event) {
        JMenuItem menu = (JMenuItem) event.getSource();
        if (menu == menuItemAdd) {
            addNewRow();
        } else if (menu == menuItemRemove) {
            removeCurrentRow();
        }
        /*else if (menu == menuItemEditCurrent) {
            //editCurrentRow();
        }*/
    }
    
    
}
