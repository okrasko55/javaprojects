/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.LocationTableModel;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author Oleg K
 */
public class LocationTableView extends JFrame{
    public LocationTableView(){
        LocationTableModel model = new LocationTableModel();
        JTable table = new JTable(model);
        
        this.add(new JScrollPane(table));

        this.setTitle("Editable Table Location");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
    }
}
