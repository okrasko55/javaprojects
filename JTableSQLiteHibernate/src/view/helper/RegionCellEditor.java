/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.helper;

import entityClasses.Region;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

/**
 *
 * @author Oleg K
 */
public class RegionCellEditor extends AbstractCellEditor implements TableCellEditor, ActionListener{

    //variables
    private Region region;
    private List<Region> listRegion;

    //constructors
    public RegionCellEditor(List<Region> listRegion) {
        this.listRegion = listRegion;
    }

    @Override
    public Object getCellEditorValue() {
        return this.region; 
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        if (value instanceof Region) {
            this.region = (Region) value;
        }
        JComboBox<Region> comboRegion = new JComboBox<Region>();

        for (Region aRegion : listRegion) {
            comboRegion.addItem(aRegion);
        }

        comboRegion.setSelectedItem(region);
        comboRegion.addActionListener(this);

        if (isSelected) {
            comboRegion.setBackground(table.getSelectionBackground());
            comboRegion.setForeground(table.getSelectionForeground());
        } else {
            comboRegion.setBackground(table.getBackground());
            comboRegion.setForeground(table.getForeground());
        }
        return comboRegion;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        JComboBox<Region> comboRegion = (JComboBox<Region>) event.getSource();
        this.region = (Region) comboRegion.getSelectedItem();
    }
    
}
