/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import entityClasses.Country;
import entityClasses.Region;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JTextField;
import model.dao.BaseDaoImpl;
import model.dao.interfaces.BaseDaoInterface;

/**
 *
 * @author Oleg K
 */
public class CountryDialogView extends JDialog{
    //variables
    private JButton buttonCreate = new JButton("Create");
    private JButton buttonCancel = new JButton("Cancel");
//    private JTextField fieldCountryId = new JTextField(25);
    private JTextField fieldCountryName = new JTextField(25);
    private Region selectedRegion;
    public Country country;
    
    //constructors
    public CountryDialogView(Frame parent, Country country){
        super(parent, "Add New Country", true);
        this.country = country;
        setLayout(new FlowLayout(FlowLayout.LEFT, 10, 10));
        
        //creating combobox
        JComboBox<Region> comboRegion = new JComboBox<>();
        List<Region> regions = new BaseDaoImpl<>(Region.class).getAll();
        for (Region region : regions) {
            comboRegion.addItem(region);
        }
        
        // customize some appearance:
        comboRegion.setForeground(Color.BLUE);
        comboRegion.setFont(new Font("Arial", Font.BOLD, 14));
        comboRegion.setMaximumRowCount(10);
        
        // make the combo box editable so we can add new item when needed
        comboRegion.setEditable(false);
        comboRegion.addActionListener(event -> {
            JComboBox<Region> combo = (JComboBox<Region>) event.getSource();
            selectedRegion = (Region) combo.getSelectedItem();
            country.setRegion(selectedRegion);
            country.setCountryName(fieldCountryName.getText());
//            try{
//                country.setCountryId(Integer.parseInt(fieldCountryId.getText()));
//            }catch(NumberFormatException e){
//                System.out.println("Not a number");
//            }
        });
                // add event listener for the button Select 
        buttonCreate.addActionListener(event -> {
                new BaseDaoImpl<>(Country.class).add(country);
                setVisible(false);
        });
        
        // add event listener for the button Remove
        buttonCancel.addActionListener(event -> setVisible(false));
        
        // add components to this frame
//        add(fieldCountryId);
        add(fieldCountryName);
        add(comboRegion);
        add(buttonCreate);
        add(buttonCancel);

        pack();
        setResizable(false);
        setLocationRelativeTo(parent);
    }
    
}
