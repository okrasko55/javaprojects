/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtablesqlitehibernate;

import entityClasses.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.SwingUtilities;
import model.dao.BaseDaoImpl;
import model.dao.LocationDaoImpl;
import model.dao.interfaces.BaseDaoInterface;
import model.dao.interfaces.LocationDaoInterface;
import view.CountryTableView;
import view.LocationTableView;
/**
 *
 * @author Oleg K
 */
public class JTableSQLiteHibernate {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
//        LocationDaoInterface locationDao = new LocationDaoImpl();
//        List<Location> list = locationDao.getAll();
//        Location loc = new Location(1L, "5", "11000", "NYC", "111", "1");
//        locationDao.updateLocation(loc, loc.getLocationId());
        SwingUtilities.invokeLater(() -> new LocationTableView());
        SwingUtilities.invokeLater(() -> new CountryTableView());
        
    }
    
}
