<%-- 
    Document   : EmployeesSelectResponse
    Created on : 19.08.2017, 19:23:53
    Author     : asp
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>


<sql:setDataSource var="snapshot" driver="org.sqlite.JDBC"
     url="jdbc:sqlite:d:\MyData\test.db"
     user="root"  password="pass123"/>

<sql:query dataSource="${snapshot}" var="result">
SELECT em.*,d.Name as DepName from Employees em left join EmployeesToDepartments etod on em.EmployeesId = etod.EmployeesId left join Departments d on d.DepartmentsId = etod.DepartmentsId ;
</sql:query>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page for Employees</title>
    </head>
    <body>
        <h1>Список сотрудников :</h1>
        <table border="1" width="100%">
            <tr>
                <th>Emp ID</th>
                <th>Name</th>
                <th>id Code</th>
                <th>Deps Namee</th>
            </tr>
            <c:forEach var="row" items="${result.rows}">
                <tr>
                    <td><c:out value="${row.EmployeesId}"/></td>
                    <td><c:out value="${row.Employees_Name}"/></td>
                    <td><c:out value="${row.IdCode}"/></td>
                    <td><c:out value="${row.DepName}"/></td>
                </tr>
            </c:forEach>
        </table>
        <form action="index.jsp">
            <input type="submit" value="home" />
        </form>
    </body>
</html>

