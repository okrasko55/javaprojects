<%-- 
    Document   : welcome
    Created on : 12.08.2017, 19:10:30
    Author     : asp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Welcome Page</title>
    </head>
    <body>
        <h1>Hello, 
            <%
                String name = request.getParameter("name");
                if (name == null || name.length() == 0) {
            %>
            Hello, world !
            <%            } else {
            %>
            Hello, world ! I'm <%= name%>
            <%
                }
            %>
        </h1>
        <% if (Math.random() < 0.5) { %>
        <B>Удачного</B> Вам дня!
            <% } else { %>
        <B>Не удачного</B> Вам дня!
            <% }%>

        <h2>Список таблиц</h2>
        <table border="0">
            <thead>
                <tr>
                    <th><strong>Имя таблицы</strong> </th>
                    <th colspan="2"><strong>Операции</strong></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Employees (Сотрудники)</td>
                    <td><form method="post" action="index.jsp">
                            <input type="submit" value="Список" name="btnAction1" />
                        </form></td>
                    <td>
                    </td>
                    <td><form method="get" action="index.jsp">
                            <input type="submit" value="на Главную" name="btnAction2" />
                        </form></td>
                </tr>
                <tr>
                    <td>Departments (Подразделения)</td>
                    <td><form method="post" action="index.jsp">
                            <input type="submit" value="Список" name="btnAction1" />
                        </form></td>
                    <td>
                    </td>
                    <td><form method="get" action="index.jsp">
                            <input type="submit" value="на Главную" name="btnAction2" />
                        </form></td>
                </tr>
            </tbody>
        </table>        

    </body>
</html>
