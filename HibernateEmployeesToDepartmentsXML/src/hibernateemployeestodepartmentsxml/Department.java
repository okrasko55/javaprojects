/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hibernateemployeestodepartmentsxml;

import java.util.Set;

public class Department {

    private long departmentID;

    public long getDepartmentID() {
        return departmentID;
    }

    public void setDepartmentID(long departmentID) {
        this.departmentID = departmentID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    private Set<Employee> employees;

    public Set<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<Employee> artists) {
        this.employees = artists;
    }

    //
    //Constructors,  Accessors and mutators for all four fields 
    public Department() {
    }

    public Department(String name) {
        this.name = name;
    }

    public String getEmployees_Name() {
        return name;
    }

    public void setEmployees_Name(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Department{"
                + "id=" + departmentID
                + ", name='" + name + '\''
                + '}';
    }
}
