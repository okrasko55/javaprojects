/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hibernateemployeestodepartmentsxml;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author asp
 */
public class HibernateEmployeesToDepartmentsXML {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        testDepartmentEx();
    }

    private static void testDepartmentEx() throws ExceptionInInitializerError, HibernateException {
        SessionFactory mFctory;
        try {
            mFctory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Couldn't create session factory." + ex);
            throw new ExceptionInInitializerError(ex);
        }
        Session session = null;
        session = mFctory.openSession();
        Transaction tx = null;
        Long departmentID = null;
        String name = "Dep#3";
        try {
            tx = session.beginTransaction();
            Department department = new Department(name);
            //
            Set<Employee> empls = new HashSet<Employee>(2);
            Employee e1 = new Employee("Иванов");
            session.save(e1);
            Employee e2 = new Employee("Иванов 1");
            session.save(e2);
            empls.add(e1);
            empls.add(e2);
            department.setEmployees(empls);
            //
            departmentID = (Long) session.save(department);
            tx.commit();
            System.out.println(String.format("Inserted: %s\n", departmentID));
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        session = mFctory.openSession();
        System.out.println("--- Find all Departments && Employees ---");
        Query query = session.createQuery("SELECT d FROM Department d");
        List<Department> departments = query.list();
        for (Department foundDepartment : departments) {
            System.out.println(String.format("F: %s\n", foundDepartment));
            //
            for (Employee employee : foundDepartment.getEmployees()) {
                System.out.println(String.format("departments employees: %s", employee));
            }
        }
        //
        session.close();
        //
    }

}
