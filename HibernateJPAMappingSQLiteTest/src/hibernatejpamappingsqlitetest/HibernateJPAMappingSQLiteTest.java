/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hibernatejpamappingsqlitetest;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author asp
 */
public class HibernateJPAMappingSQLiteTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        testEmployee("Ivan");
//        testSelectEmployee();
//        testEmployees("Sidor", 3);
//        testSelectEmployees();
        testUser("Ivan", "Ivanov");
        testSelectUser();
    }
    
    private static void testEmployee(String name) throws HibernateException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Employee emp = new Employee();
        emp.setName(name);
        session.save(emp);
        session.getTransaction().commit();
        session.close();
    }
    
    private static void testSelectEmployee() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        String hql = "from Employee";
        Query query = session.createQuery(hql);
        List<Employee> listEmployee = query.list();
        for (Employee employee : listEmployee) {
            System.out.println(employee.getName());
        }
        session.close();
    }

    private static void testEmployees(String name, int i) throws HibernateException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Employees emp = new Employees();
        emp.setEmployees_Name(name);
        emp.setIdCode(i);
        session.save(emp);
        session.getTransaction().commit();
        session.close();
    }   
    private static void testSelectEmployees() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        String hql = "from Employees";
        Query query = session.createQuery(hql);
        List<Employees> listEmployee = query.list();
        for (Employees employee : listEmployee) {
            System.out.println(employee.getEmployeesId() + employee.getIdCode() + employee.getEmployees_Name());
        }
        session.close();
    }
    
    private static void testUser(String fName, String lName) throws HibernateException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        User user = new User();
        user.setFirstName(fName);
        user.setLastName(lName);
        session.save(user);
        session.getTransaction().commit();
        session.close();
    }   
    private static void testSelectUser() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        String hql = "from User";
        Query query = session.createQuery(hql);
        List<User> listUser = query.list();
        for (User user : listUser) {
            System.out.println(user.getUserId() + " " + user.getFirstName() + " " + user.getLastName());
        }
        session.close();
    }
}
